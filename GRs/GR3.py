"""CS 210, Introduction to Programming, Fall 2015, Tylar Hanson.

Instructor: Dr. Bower

Documentation: No help received; this GR is entirely individual effort.
=======================================================================
"""

import easygui
import math

""" The following code segments are provided for your reference.
    You are free to copy/paste them into your answers below.

    # An easygui message box to display a formatted string.
    easygui.msgbox( "pi = {:.2f}".format( 22 / 7 ), "Result" )

    # An easygui enter box for entering a string.
    s = easygui.enterbox( "Enter a string:", "Input" )

    # An easygui integer box for entering a positive integer.
    n = easygui.integerbox( "Enter a positive integer:", "Input", "", 1, 2 ** 31 )

    # Get a file name from the user with an easygui File Open Dialog.
    filename = easygui.fileopenbox( default="./data/*.txt" )

    # Read the entire contents of filename into a single string.
    with open( filename ) as data_file:
        data_string = data_file.read()

    # Read the entire contents of filename into a list of strings, one per word.
    with open( filename ) as data_file:
        data_words = data_file.read().split()

    # Read the entire contents of filename into a list of strings, one per line.
    with open( filename ) as data_file:
        data_lines = data_file.read().splitlines()
"""


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print( __doc__ )

    # Call each individual exercise; comment/un-comment these lines as you work.
    problem1()
    # problem2()
    # problem3()
    # problem4()
    # problem5()
    # problem6()
    # problem7()
    # problem8()


def problem1():
    """Uses the specified function as described in the exam document."""
    # 1b: Write code to use the function as described in the exam document.
    words = longest_words( "./data/Test.txt")
    for word in words:
        print( word )

    words2 = longest_words( easygui.fileopenbox( "Open a file", "Input", "./data/*.txt" ) )
    for word in words2:
        print( word )


# 1a: In the space below, write the function as described in the exam document.
def longest_words( filename ):
    result = []
    with open( filename ) as data_file:
        lines = data_file.read().splitlines()
    for line in lines:
        longest = ''
        for word in line.split( sep=" " ):
            if len( word ) > len( longest ):
                longest = word
        result.append( longest )
    return result


def problem2():
    """Uses the specified function as described in the exam document."""
    # TODO 2b: Write code to use the function as described in the exam document.
    print( "Inches   Yards")
    print( "======  =======")
    for i in range( 30, 480, 30 ):
        print( "{:>5d}   {:>7.4f}".format( i, inches_to_yards( i ) ) )


# 2a: In the space below, write the function as described in the exam document.
def inches_to_yards( inches ):
    return inches / 36.0


def problem3():
    """Uses the specified function as described in the exam document."""
    # 3b: Write code to use the function as described in the exam document.
    print( min_within( [23, 67, 15, 23, 18, 42, 14, 89], 3, 5 ) )
    print( min_within( [11, 42, 78, 24, 13, 85, 27], 1, 4 ) )
    print( min_within( [22, 58, 12, 10], -2, 2 ) )
    print( min_within( [42, 17, 86, 29, 33], 2, 6 ) )


# 3a: In the space below, write the function as described in the exam document.
def min_within( list, first, last ):
    if first < 0:
        first = 0
    if last > len( list ):
        last = len( list ) - 1
    smallest = list[ first ]
    for i in range( first, last + 1 ):
        if list[i] < smallest:
            smallest = list[i]
    return smallest


def problem4():
    """Uses the specified function as described in the exam document."""
    # 4b: Write code to use the function as described in the exam document.
    print( rev_str( 'USAFA', len( 'USAFA' ) ) )
    print( rev_str( 'CS 210', len( 'CS 210' ) ) )
    print( rev_str( 'race car', len( 'race car' ) ) )
    string = easygui.enterbox( "Enter something", "Input" )
    easygui.msgbox( rev_str( string, len( string ) ) )


# 4a: In the space below, write the function as described in the exam document.
def rev_str( string, depth ):
    if depth > 0:
        return string[-1] + str( rev_str( string[:-1], depth - 1 ) )
    else:
        return ''


def problem5():
    """Uses the specified function as described in the exam document."""
    # 5b: Write code to use the function as described in the exam document.
    board = build_board( 12, 'A', 'Z' )
    for row in board:
        print( " ".join( row ) )


# 5a: In the space below, write the function as described in the exam document.
def build_board( size, letter1, letter2 ):
    board = []
    for row in range( 3 ):
        row_list = [ '-' if row % 2 == 0 else letter1 ]
        while len( row_list ) < size:
            row_list.append( letter1 if row_list[-1] == '-' else '-' )
        board.append( row_list )

    if size > 6:
        for _ in range( size - 6 ):
            row = []
            for _ in range( size ):
                row.append( '-' )
            board.append( row )

    for row in range( 3 ):
        row_list = [ letter2 if row % 2 == 0 else '-' ]
        while len( row_list ) < size:
            row_list.append( letter2 if row_list[-1] == '-' else '-' )
        board.append( row_list )

    return board

def problem6():
    """Uses the specified function as described in the exam document."""
    # 6b: Write code to use the function as described in the exam document.
    for number in range( 10000, 0, -1):
        if is_perfect_num( number ):
            print( number )


# 6a: In the space below, write the function as described in the exam document.
def get_factors( number ):
    factors = []
    for value in range( number - 1, 0, -1 ):
        if number % value == 0:
            factors.append( value )
    return factors


def is_perfect_num( number ):
    if sum( get_factors( number ) ) == number:
        return True
    else:
        return False


def problem7():
    """Uses the specified class as described in the exam document."""
    # 7b: Write code to use the class as described in the exam document.
    alarm_0515 = Alarm( 5, 15 )
    print(alarm_0515)

    alarm_1630 = Alarm( 16, 30 )
    print(alarm_1630)

    alarm_0915 = Alarm( 8, 75 )
    print(alarm_0915)

    alarm_0105 = Alarm( 23, 125 )
    print(alarm_0105)

    alarm_0515 = Alarm( 5, 15)
    print( alarm_0515 )
    alarm_0515.snooze( 10 )
    print( alarm_0515 )

    alarm_0545 = Alarm( 5, 45 )
    print( alarm_0545 )
    alarm_0545.snooze( 20 )
    print( alarm_0545 )


# 7a: In the space below, write the class as described in the exam document.
class Alarm():

    def __init__( self, hour=6, minute=45 ):
        self.hour = ( hour + minute // 60 ) % 24
        self.minute = minute % 60

    def __str__(self):
        if self.hour < 10:
            if self.minute < 10:
                return "0{}0{}".format(self.hour, self.minute)
            else:
                return "0{}{}".format(self.hour, self.minute)
        else:
            if self.minute < 10:
                return "{}0{}".format(self.hour, self.minute)
            else:
                return "{}{}".format(self.hour, self.minute)

    def snooze(self, minutes):
        self.minute += minutes
        self.hour += ( self.minute // 60 ) % 24
        self.minute %= 60


def problem8():
    """Uses the specified classes as described in the exam document."""
    # 8b: Write code to use the classes as described in the exam document.
    containers = [ Box( 'wine', 13, 14, 15 ), Sphere( 'milk', 10 ), Cylinder( 'beer', 8, 12 ) ]
    for container in containers:
        print( container )
    print()
    containers.sort()
    for container in containers:
        print( container )


# 8a: In the space below, write the classes as described in the exam document.
class Container( object ):

    def __init__( self, contents ):
        self.contents = contents

    def __str__( self ):
        return "{:.2f} units of {}".format( self.volume(), self.contents )

    def __lt__( self, other ):
        if self.volume() < other.volume():
            return True
        else:
            return False


class Box( Container ):

    def __init__( self, contents='water', width=0, height=0, depth=0 ):
        super().__init__( contents )
        self.width = width
        self.height = height
        self.depth = depth

    def volume(self):
        return self.width * self.height * self.depth


class Sphere( Container ):

    def __init__( self, contents='water', radius=0 ):
        super().__init__( contents )
        self.radius = radius

    def volume(self):
        return 4 / 3 * math.pi * self.radius ** 3


class Cylinder( Container ):

    def __init__( self, contents='water', radius=0, height=0 ):
        super().__init__( contents )
        self.radius = radius
        self.height = height

    def volume(self):
        return math.pi * self.radius ** 2 * self.height


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
