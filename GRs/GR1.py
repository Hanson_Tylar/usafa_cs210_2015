"""CS 210, Introduction to Programming, Fall 2015, Tylar Hanson.

Instructor: Dr. Bower

Documentation: No help received; this GR is entirely individual effort.
=======================================================================
"""

import easygui
import random
import math
import turtle

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960               # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 // 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = WIDTH // 30      # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = MARGIN // 2   # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True          # Set to True for fast, non-animated turtle movement.


""" The following code segments are provided for your reference.
    You are free to copy/paste them into your answers below.

    # An easygui message box to display a formatted string.
    easygui.msgbox( "pi = {:.2f}".format( 22 / 7 ), "Result" )

    # An easygui integer box for entering a string.
    s = easygui.enterbox( "Enter a string:", "Input" )

    # An easygui integer box for entering a positive integer.
    n = easygui.integerbox( "Enter a positive integer:", "Input", "", 1, 2 ** 31 )

    # Read the entire contents of a file into a single string.
    with open( easygui.fileopenbox( default="./data/*.txt" ) ) as data_file:
        data_string = data_file.read()

    # Read the entire contents of a file into a list of strings, one per word.
    with open( easygui.fileopenbox( default="./data/*.txt" ) ) as data_file:
        data_words = data_file.read().split()

    # Read the entire contents of a file into a list of strings, one per line.
    with open( easygui.fileopenbox( default="./data/*.txt" ) ) as data_file:
        data_lines = data_file.read().splitlines()
"""


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print( __doc__ )

    # Call each individual exercise; comment/un-comment these lines as you work.
    # problem1()
    # problem2()
    # problem3()
    problem4()
    # problem5()
    # problem6()


def problem1():
    """Uses the specified function as described in the exam document."""
    # 1b: Write code to use the function as described in the exam document.
    print("Kilometers  Miles")
    print("==========  =======")
    for value in range(1000, 5001, 500):
        print("{:7d}     {:.2f}".format(value, kilometers_to_miles(value)))


# 1a: In the space below, write the function as described in the exam document.
def kilometers_to_miles( kilometers ):
    return kilometers * .621371


def problem2():
    """Uses the specified function as described in the exam document."""
    # 2b: Write code to use the function as described in the exam document.
    easygui.msgbox("The difference between the sum of odds and sum of evens is {}".
                   format(even_odd(10, 10, 99), "Result"))


# 2a: In the space below, write the function as described in the exam document.
def even_odd(how_many, lower_bound, upper_bound):
    count = 0
    sum_evens = 0
    sum_odds = 0
    while count < how_many:
        number = random.randint(lower_bound, upper_bound)
        if number % 2 == 0:
            sum_evens += number
        else:
            sum_odds += number
        count += 1

    return sum_evens - sum_odds


def problem3():
    """Uses the specified function as described in the exam document."""
    # 3b: Write code to use the function as described in the exam document.
    a = easygui.integerbox(msg="Enter first value:", title="Input" )
    b = easygui.integerbox(msg="Enter second value:", title="Input" )
    c = easygui.integerbox(msg="Enter third value:", title="Input" )
    d = easygui.integerbox(msg="Enter fourth value:", title="Input" )
    e = gcd( a, b )
    f = gcd( c, d )
    final = gcd( e, f)
    easygui.msgbox(msg="The GCD of {}, {}, {}, and {} is {}.".format(a, b, c, d, final))


# 3a: In the space below, write the function as described in the exam document.
def gcd(first_value, second_value):
    """Determines the greatest common divisor of two positive integers.

    :param int first_value: First of two numbers.
    :param int second_value: Second of two numbers.
    :return int: GCD of first and second number.
    """

    while first_value != second_value:
        if first_value > second_value:
            first_value -= second_value
        else:
            second_value -= first_value

    return first_value


def problem4():
    """Uses the specified function as described in the exam document."""
    # TODO 4b: Write code to use the function as described in the exam document.
    filename = easygui.fileopenbox(msg="Select af file:", title="Input", default='*')
    # Need to read the file
    male_words = [ "he", "him" ]
    female_words = [ "she", "her"]
    male_count = count_word( male_words, filename)
    female_count = count_word(female_words, filename)
    if male_count > female_count:
        easygui.msgbox("There are more male than female pronouns")
    elif male_count < female_count:
        easygui.msgbox("There are more female than male pronouns")
    else:
        easygui.msgbox("There are equal male than female pronouns")


# TODO 4a: In the space below, write the function as described in the exam document.
def count_word(word_to_count, words_to_be_counted):
    count = 0
    # Need to add loop to start at beginning of file and end after last word is compared
    if word_to_count in words_to_be_counted:
        count += 1

    return count


def problem5():
    """Use the screen and turtle defined below to solve the problem specified in the exam document."""
    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()

    # 5b: Write code to use the function as described in the exam document.
    r = ( WIDTH - MARGIN * 4 ) / 6
    x = -( 2 * r + MARGIN)
    y = 1 / 3 * r
    for color in [ "blue", "yellow", "black", "green", "red" ]:
        artist.color( color )
        draw_circle(artist, x, y, r)
        x += r + 1 / 2 * MARGIN
        if y > 0:
            y -= 2 / 3 * r
        else:
            y += 2 /3 * r

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


#  5a: In the space below, write the function as described in the exam document.
def draw_circle( tom, x, y, r ):
    tom.pu()
    tom.goto( x, y )
    tom.right(90)
    tom.forward( r )
    tom.left(90)
    tom.pd()
    tom.circle( r )


def problem6():
    """Use the screen and turtle defined below to solve the problem specified in the exam document."""
    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()

    # 6: Write code to solve the problem as described in the GR document.
    r_outside = ( HEIGHT - 2 * MARGIN ) / 2
    r_inside = r_outside / 2
    draw_circle( artist, 0, 0, r_outside )
    draw_circle( artist, 0, 0, r_inside )
    hit = 0
    miss = 0
    while hit < 7 and miss < 7:
        x = random.randrange( -WIDTH / 2, WIDTH / 2)
        y = random.randrange( -HEIGHT / 2, HEIGHT / 2)
        artist.pu()
        artist.goto( x, y )
        distance_to_center = math.sqrt( x ** 2 + y ** 2)
        if distance_to_center < r_inside:
            artist.dot( 32, "red")
            hit += 2
        elif distance_to_center < r_outside:
            artist.dot( 32, "green")
            hit += 1
        else:
            artist.dot( 32, "yellow")
            miss += 1

    if hit > miss:
        writer.write("Winner!", align="center", font=("Arial", FONT_SIZE, "normal"))
    else:
        writer.write("Try Again", align="center", font=("Arial", FONT_SIZE, "normal"))



    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup( WIDTH, HEIGHT, MARGIN, MARGIN )
    screen.bgcolor( "SkyBlue" )

    # Create two turtles, one for drawing and one for writing.
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape( "turtle" )

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay( 0 )
        artist.hideturtle()
        artist.speed( "fastest" )
        writer.hideturtle()
        writer.speed( "fastest" )

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading( 90 )   # Straight up, which makes it look sort of like a cursor.
    writer.penup()            # A turtle's pen does not have to be down to write text.
    writer.setposition( 0, HEIGHT // 2 - FONT_SIZE * 2 )  # Centered at top of the screen.

    return screen, artist, writer


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
