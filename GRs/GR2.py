"""CS 210, Introduction to Programming, Fall 2015, Tylar Hanson.

Instructor: Dr. Bower

Documentation: No help received; this GR is entirely individual effort.
=======================================================================
"""

import easygui
import random

""" The following code segments are provided for your reference.
    You are free to copy/paste them into your answers below.

    # An easygui message box to display a formatted string.
    easygui.msgbox( "pi = {:.2f}".format( 22 / 7 ), "Result" )

    # An easygui enter box for entering a string.
    s = easygui.enterbox( "Enter a string:", "Input" )

    # An easygui integer box for entering a positive integer.
    n = easygui.integerbox( "Enter a positive integer:", "Input", "", 1, 2 ** 31 )

    # Get a file name from the user with an easygui File Open Dialog.
    filename = easygui.fileopenbox( default="./data/*.txt" )

    # Read the entire contents of filename into a single string.
    with open( filename ) as data_file:
        data_string = data_file.read()

    # Read the entire contents of filename into a list of strings, one per word.
    with open( filename ) as data_file:
        data_words = data_file.read().split()

    # Read the entire contents of filename into a list of strings, one per line.
    with open( filename ) as data_file:
        data_lines = data_file.read().splitlines()
"""


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print( __doc__ )

    # Call each individual exercise; comment/un-comment these lines as you work.
    # problem1()
    problem2()
    # problem3()
    # problem4()


def problem1():
    """Uses the specified function as described in the exam document."""
    # 1b: Write code to use the function as described in the exam document.
    winner = generate_lotto( 24 )
    ticket = generate_lotto( 24 )
    print("Winner = {}".format( winner ) )
    print("Ticket = {}".format( ticket ) )
    count = 0
    for value in ticket:
        if value in winner:
            count += 1
    print( "Count = {}".format( count ) )


# 1a: In the space below, write the function as described in the exam document.
def generate_lotto( upper_bound ):
    lotto_list = []
    i = 0
    while i < 6:
        num = random.randint(1, upper_bound)
        if num not in lotto_list:
            lotto_list.append( num )
            i += 1
    return lotto_list


def problem2():
    """Uses the specified function as described in the exam document."""
    # 2b: Write code to use the function as described in the exam document.
    matrix = build_matrix( 8, 7 )
    string = ""
    for r in range(8):
        for c in range(7):
            string += str( matrix[r][c] ) + "   "
        string += "\n"
    print( string )


# TODO 2a: In the space below, write the function as described in the exam document.
def build_matrix( r, c ):
    row = []
    new_row = []
    matrix = []
    for r in range(r):
        for c in range(c):
            if r != 0 or c != 0:
                # Add value above and to the left of new entry
                new_row.append(matrix[r - 1][c] + matrix[r][c - 1])
            # If top row or very left column put in a 1
            else:
                new_row.append(1)
        row = new_row
        matrix.append( row )

    return matrix


def problem3():
    """Uses the specified function as described in the exam document."""
    # 3b: Write code to use the function as described in the exam document.
    filename = easygui.fileopenbox("Choose file to print in a box", "Input", "./data/Test.txt")
    print_in_box( filename )


# 3a: In the space below, write the function as described in the exam document.
def print_in_box( filename ):
    with open( filename ) as data_file:
        lines = data_file.read().splitlines()
        longest = ""
        for line in lines:
            if len( line ) > len( longest ):
                longest = line
        print("+", "-" * len( longest ), "+")
        for line in lines:
            print("| {:>{}} |".format( line, len( longest ) ) )
        print("+", "-" * len( longest ), "+")


def problem4():
    """Uses the specified function as described in the exam document."""
    # 4b: Write code to use the class as described in the exam document.
    # Room to spare
    bucket = Bucket( 50, 0)
    print( "Bucket before filling is {}".format( bucket ) )
    other = Bucket( 100, 25)
    Bucket.fill_from( bucket, other )
    print( "First bucket is {}".format( bucket ) )
    # print( "\tOther bucket is {}".format( other ) )
    # Fills exactly
    bucket = Bucket( 50, 0)
    print( "Bucket before filling is {}".format( bucket ) )
    other = Bucket( 100, 50)
    Bucket.fill_from( bucket, other )
    print( "First bucket is {}".format( bucket ) )
    # print( "\tOther bucket is {}".format( other ) )
    # More than enough
    bucket = Bucket( 50, 0)
    print( "Bucket before filling is {}".format( bucket ) )
    other = Bucket( 100, 100)
    Bucket.fill_from( bucket, other )
    print( "First bucket is {}".format( bucket ) )
    # print( "\tOther bucket is {}".format( other ) )

# 4a: In the space below, write the class as described in the exam document.
class Bucket():

    def __init__( self, capacity=10, fill_level=0 ):
        self.capacity = int( capacity )
        self.fill_level = int( fill_level )

    def __str__( self ):
        return str( "{} of {} full".format( self.fill_level, self.capacity ) )

    def fill_from( self, other ):
        if other.fill_level <= self.capacity - self.fill_level:
            self.fill_level += other.fill_level
            other.fill_level = 0
        else:
            remainder = other.fill_level - ( self.capacity - self.fill_level)
            self.fill_level += remainder
            other.fill_level -= remainder



# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
