"""CS 210, Introduction to Programming, Fall 2015, Tylar Hanson.

Instructor: Dr. Bower

Documentation: None
=======================================================================
"""

import easygui


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print( __doc__ )

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    # exercise1()
    # exercise2()
    # exercise3()
    exercise4()


def exercise0():
    """Demonstrate various methods of reading the contents of a data file."""
    # In the folder containing this Python program is a data folder with the file Test.txt.
    filename = "./data/Test.txt"

    # Example 1: Read the entire contents of a file into a single string.
    with open( filename ) as data_file:
        data_string = data_file.read()
    # Un-indent after reading the file so the with construct will close the file.
    print( "Example 1:", data_string, sep="\n" )

    # Example 2: Read the entire contents of a file into a list of strings, one per word.
    with open( filename ) as data_file:
        data_words = data_file.read().split()
    print( "Example 2:", data_words, sep="\n" )

    # Example 3: Read the entire contents of a file into a list of strings, one per line.
    with open( filename ) as data_file:
        data_lines = data_file.read().splitlines()
    print( "Example 3:", data_lines, sep="\n" )

    # Example 4: Uses file open dialog to select a txt file from the data folder.
    # filename = easygui.fileopenbox( default="./data/*.txt" )
    # with open( filename ) as data_file:
    #     data_string = data_file.read()
    #
    # # The data_string can be split into words and lines _without_re-reading_ the file:
    # data_words = data_string.split()
    # data_lines = data_string.splitlines()
    # print( "Example 4:", data_string, data_words, data_lines, sep="\n" )


def exercise1():
    """Reads an entire file into a string, then estimates the number of words per sentence."""
    # 1b: Write code to use the count_char function as described in the lab document.
    filename = "./data/Test.txt"
    with open( filename ) as data_file:
        data_string = data_file.read()
    words = len( data_string.split() )
    sentences = count_char( '.', filename) + count_char( '!', filename) + count_char( '?', filename)
    words_per_sentence = words / sentences
    easygui.msgbox("{} / {} = {} words per sentence.".format(words, sentences, words_per_sentence))


# 1a: In the space below, write the count_char function as described in the lab document.
def count_char(symbol, string):
    """

    :param symbol: The character to be counted.
    :param string: The string to be searched.
    :return: Integer of the count of occurrences of the character within the string.
    """

    count = 0
    while count <= len(string):
        for symbol in string:
            if symbol == string[count]:
                count += 1
            return count


def exercise2():
    """Reads a data file of Python keywords, then counts how many appear in a Python source file."""
    # TODO 2b: Write code to use the count_words function as described in the lab document.
    words_to_count = "./data/Keywords.txt"
    with open( words_to_count ) as data_file:
        data_string_2 = data_file.read().split()
    source = "Easyguidemo.py"
    with open( source ) as data_file:
        data_string_1 = data_file.read().split()
    total_words = len( data_string_1 )
    word_count = count_words(data_string_1, data_string_2)
    easygui.msgbox("{} of {} words were Python keywords, {:.2f}%"
                   .format(word_count, total_words, word_count / total_words * 100))


# TODO 2a: In the space below, write the count_words function as described in the lab document.
def count_words( words_to_count, data_words ):
    """

    :param list[str] words_to_count: The list of words to be counted.
    :param list[str] data_words: The list of words in which words from the first list are to be counted.
    :return: An integer which is the number of times words from the first list appear in the second list.
    """

    count = 0
    while True:
        for word in data_words:
            if word in words_to_count:
                count += 1
            return count


def exercise3():
    """Display file information until the user clicks Cancel."""
    # 3b: Write code to use the file_info function as described in the lab document.
    file_name = easygui.fileopenbox("./data/*.txt")
    with open( file_name ) as data_file:
        file_name = data_file.read()
    easygui.msgbox(file_info(file_name))


# 3a: In the space below, write the file_info function as described in the lab document.
def file_info(file_name):
    """

    :param str file_name: Name of file to get information about.
    :return: Information about the file.
    """

    lines = len(file_name.splitlines())
    words = len(file_name.split())
    characters = len(file_name)

    return "Lines: {}, Words: {}, Characters: {}".format(lines, words, characters)


def exercise4():
    """Display files with line numbers until the user clicks Cancel."""
    # 4b: Write code to use the print_file function as described in the lab document.
    file_name = 0
    while file_name is not None:
        file_name = easygui.fileopenbox("./data/*.txt")
        with open( file_name ) as data_file:
            file_name = data_file.read()
        print(print_file(file_name))


# 4a: In the space below, write the print_file function as described in the lab document.
def print_file(file_name):
    """

    :param file_name: The file to add line numbers to.
    """

    file_name_lines = file_name.splitlines()

    count = 0
    while count < len(file_name_lines):
        print("{:3d}: {}".format(count + 1, file_name_lines[count]))
        count += 1


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
