"""CS 210, Introduction to Programming, Fall 2015, Tylar Hanson.

Instructor: Dr. Bower

Documentation: None
=======================================================================
"""


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print( __doc__ )

    # 2b: In the space below, use the class as described in the lab document.
    books = []
    filename = "./data/Kindle.txt"
    with open( filename ) as data_file:
        lines = data_file.read().splitlines()
        for line in lines:
            title = line.split("|")[0]
            author = line.split("|")[1]
            total_pages = int( line.split("|")[2] )
            current_page = int( line.split("|")[3] )
            books.append( Book( title, author, total_pages, current_page ))
    books.sort( key=Book.progress )
    for book in books:
        print( book )


# 2a: In the space below this comment, write the class as described in the lab document.
class Book:
    """Book class to represent a book on an e-reader"""

    def __init__( self, title="Aim to Misbehave", author="Shepard Book", total_pages=100, current_page=50 ):
        """

        :param str title: Title of the book.
        :param str author: Author of the book. Default is Shepard Book
        :param int total_pages: Total number of pages in the book. Default is 100
        :param int current_page: Page number currently on. Default is 1.
        :return:
        """

        self.title = title
        self.author = author
        self.total_pages = total_pages
        self.current_page = current_page

    @property
    def total_pages( self ):
        return self.total_pages

    @total_pages.setter
    def total_pages( self, total_pages ):
        if total_pages < 0:
            raise RuntimeError( "Number of total pages must be positive")
        else:
            self.total_pages = int( total_pages )

    @property
    def current_page( self ):
        return self.current_page

    @current_page.setter
    def current_page( self, current_page ):
        if current_page < 0:
            raise RuntimeError( "Number of current page must be positive")
        elif current_page > self.total_pages:
            raise ValueError( "Number of current page cannot be greater that total pages")
        else:
            self.current_page = int( self.current_page )

    def __str__(self):
        """

        :return:
        """

        return "{}, by {}\n{}{}".format( self.title, self.author, "=" * self.progress(), "-" * (100 - self.progress()) )

    def progress(self):
        """Calculates and return the reading progress of the book.

        :return int: Reading progress of the book.
        """

        return int( self.current_page / self.total_pages * 100 )

# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
