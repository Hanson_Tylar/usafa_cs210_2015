"""CS 210, Introduction to Programming, Fall 2015, Tylar Hanson.

Instructor: Dr. Bower

Documentation: None
=======================================================================
"""

import easygui
import random
import turtle
import math


# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960               # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 // 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = WIDTH // 30      # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = MARGIN // 2   # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = False         # Set to True for fast, non-animated turtle movement.


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print( __doc__ )

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise1()
    # exercise2()
    # exercise3()
    # exercise4()
    # exercise5()
    high_low()
    # turtle_race()


def exercise1():
    """Interact with the user and test the sum_odds and sum_evens functions."""
    # You _DO_NOT_ need to modify this code for Lab 10.
    n = easygui.integerbox( "Enter n:", "Input", lowerbound=0, upperbound=2 ** 31 )
    odd = sum_odds( n )
    even = sum_evens( n )
    easygui.msgbox( "n = {}\nsum of odds = {}\nsum of evens = {}".format( n, odd, even ) )


def sum_odds( n ):
    """Calculate and return the sum of the odd numbers between 1 and n, inclusive.

    :param int n: The upper bound of the series to sum.
    :return: The sum of the odd numbers between 1 and n, inclusive.
    :rtype: int
    """
    # 1a: Re-write the code below using a while loop instead of a for loop.
    result = 0
    i = 1
    while i <= n:
        result += i
        i += 2
    return result


def sum_evens( n ):
    """Calculate and return the sum of the even numbers between 1 and n, inclusive.

    :param int n: The upper bound of the series to sum.
    :return: The sum of the even numbers between 1 and n, inclusive.
    :rtype: int
    """
    # 1b: Re-write the code below using a while loop instead of a for loop.
    result = 0
    i = 0
    while i <= n:
        result += i
        i += 2
    return result


def exercise2():
    """Interact with the user and test the summation function."""
    # You _DO_NOT_ need to modify this code for Lab 10.
    n = easygui.integerbox( "Enter n:", "Input", lowerbound=0, upperbound=2 ** 31 )

    s = summation( n, 1 )
    f = n * ( n + 1 ) // 2
    easygui.msgbox( "n = {}, summation( n, 1 ) = {}, formula result = {}".format( n, s, f ) )

    s = summation( n, 2 )
    f = n * ( n + 1 ) * ( 2 * n + 1 ) // 6
    easygui.msgbox( "n = {}, summation( n, 2 ) = {}, formula result = {}".format( n, s, f ) )

    s = summation( n, 3 )
    f = ( n * ( n + 1 ) // 2 ) ** 2
    easygui.msgbox( "n = {}, summation( n, 3 ) = {}, formula result = {}".format( n, s, f ) )


def summation( n, exponent ):
    """Calculation and return the summation of the series 1**exponent + 2**exponent + ... + n**exponent.

    :param int n: The upper bound of the series to sum.
    :param int exponent: The exponent for each term in the series.
    :return: The summation of the series.
    :rtype: int
    """
    # 2: Re-write the code below using a while loop instead of a for loop.
    result = 0
    i = 1
    while i <= n:
        result += i ** exponent
        i += 1
    return result


def exercise3():
    """Interact with the user and test the count_multiples function."""
    # You _DO_NOT_ need to modify this code for Lab 10.
    start = easygui.integerbox( "Enter start value:", "Input", "", -2 ** 31, 2 ** 31 )
    stop = easygui.integerbox( "Enter stop value:", "Input", "", -2 ** 31, 2 ** 31 )
    step = easygui.integerbox( "Enter divisor value:", "Input", "", -2 ** 31, 2 ** 31 )
    easygui.msgbox( "There are {} multiples of {} in the range [{},{}].".format(
        count_multiples( start, stop, step ), step, start, stop ), "Result" )


def count_multiples( start, stop, divisor ):
    """Count and return the number of values between start and stop, inclusive, evenly divisible by divisor.

    :param int start: The start value for the range, inclusive.
    :param int stop: The stop value for the range, inclusive.
    :param int divisor: The divisor to be counted.
    :return: The number of values in the range [start, stop] evenly divisible by divisor.
    :rtype: int
    """
    # 3: Re-write the code below using a while loop instead of a for loop.
    result = 0
    i = start
    while i <= stop:
        if i % divisor == 0:
            result += 1
        i += 1
    return result


def exercise4():
    """Interact with the user and test the count_sevens function."""
    # 4b: Write code to use the count_sevens function as described in the lab document.
    n = easygui.integerbox( msg="How many 7's?", title="Input", default="", lowerbound=0, upperbound=1000)
    easygui.msgbox("{} out of {} rolls were 7.".format(n, roll_sevens( n )), "Result")


# 4a: In the space below, write the count_sevens function as described in the lab document.
def roll_sevens( n ):
    """

    :param int n: Number of times the function should roll a seven.
    :return: Returns the total number of times the dice were rolled.
    """

    sevens = 0
    rolls = 0
    while sevens < n:
        if random.randint( 1, 6 ) + random.randint( 1, 6 ) == 7:
            sevens += 1
        rolls += 1
    return rolls


def exercise5():
    """Interact with the user and test the scorekeeper function."""
    # 5b: Write code to use the scorekeeper function as described in the lab document.

    player_1 = easygui.enterbox( msg='Enter first player name:', title='Input', default='Zoe' )
    player_2 = easygui.enterbox( msg='Enter second player name:', title='Input', default='Wash' )
    game_point = easygui.integerbox( 'Enter required winning score:', 'Input', '7', 0, 2 ** 32 )

    easygui.msgbox("{} Wins!".format(scorekeeper(player_1, player_2, game_point)))


# 5a: In the space below, write the scorekeeper function as described in the lab document.
def scorekeeper( player_1, player_2, game_point ):
    """

    :param str player_1: Name of first player.
    :param str player_2: Name of second player.
    :param game_point: Score to end the game at.
    :return:
    """

    player_1_score = 0
    player_2_score = 0

    while player_1_score < game_point and player_2_score < game_point \
            or math.fabs(player_1_score - player_2_score) <= 1:
        point_goes_to = easygui.buttonbox("{}: {} \n {:^12} \n {}: {} \n\n Who wins current point?"
                                          .format(player_1, player_1_score, "vs.", player_2, player_2_score), 'Input',
                                          [ player_1, player_2 ] )

        if point_goes_to == player_1:
            player_1_score += 1
        else:
            player_2_score += 1

    if player_1_score > player_2_score:
        return player_1
    else:
        return player_2


def high_low():
    """Implement the number guessing game High Low."""
    # Create a random number in the range [1,100] for the user to guess.
    secret_number = 23  # random.randint( 1, 100 )
    # For debugging purposes only, it's nice to know the secret.
    print( secret_number, flush=True )  # Add the flush to ensure there's no buffering.

    # TODO 6: Implement the High Low guessing game as described in the lab document.

    guess = easygui.integerbox( "Enter a guess:", "Input", "", 1, 100 )
    count = 1

    while guess != secret_number:

        count += 1
        if guess > secret_number:
            easygui.msgbox( "Your guess of {} is too high.".format( guess ), "Result" )
            guess = easygui.integerbox( "Enter a guess:", "Input", "", 1, 100 )
        if guess < secret_number:
            easygui.msgbox( "Your guess of {} is too low.".format( guess ), "Result" )
            guess = easygui.integerbox( "Enter a guess:", "Input", "", 1, 100 )

    easygui.msgbox( "You win! You guessed {} in {} guesses.".format( secret_number, count ), "Result" )


def turtle_race():
    """Implement a simple turtle race from left to right across the screen."""
    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()

    # Rename the artist turtle and move her to the left, above the x-axis.
    flojo = artist  # Flo-Jo, https://en.wikipedia.org/wiki/Florence_Griffith_Joyner
    flojo.shape( "turtle" )
    flojo.color( "blue" )  # USA!
    flojo.penup()
    flojo.setposition( -WIDTH // 2 + MARGIN, MARGIN * 2 )
    flojo.setheading( 0 )
    flojo.pendown()

    # Create a new turtle, below the x-axis, to race against the turtle formerly known as artist.
    usain = turtle.Turtle() # Usain Bolt, https://en.wikipedia.org/wiki/Usain_Bolt
    usain.shape( "turtle" )
    usain.color( "green" )  # Jamaica
    usain.penup()
    usain.setposition( -WIDTH // 2 + MARGIN, -MARGIN * 2 )
    usain.setheading( 0 )
    usain.pendown()

    # TODO 7: Implement the turtle race as described in the lab document.
    writer.write( "And they're off . . .", align="center", font=( "Times", FONT_SIZE, "bold" ) )
    while True:
        flojo.forward( random.randint( MARGIN // 4, MARGIN ) )
        usain.forward( random.randint( MARGIN // 4, MARGIN ) )

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup( WIDTH, HEIGHT, MARGIN, MARGIN )
    screen.bgcolor( "SkyBlue" )

    # Create two turtles, one for drawing and one for writing.
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape( "turtle" )

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay( 0 )
        artist.hideturtle()
        artist.speed( "fastest" )
        writer.hideturtle()
        writer.speed( "fastest" )

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading( 90 )   # Straight up, which makes it look sort of like a cursor.
    writer.penup()            # A turtle's pen does not have to be down to write text.
    writer.setposition( 0, HEIGHT // 2 - FONT_SIZE * 2 )  # Centered at top of the screen.

    return screen, artist, writer


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
