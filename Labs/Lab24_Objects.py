"""CS 210, Introduction to Programming, Fall 2015, Tylar Hanson.

Instructor: Dr. Bower

Documentation: None required; cooperation on labs is highly encouraged!
=======================================================================
"""

import math
import random
import turtle

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960               # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 // 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = WIDTH // 30      # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = MARGIN // 2   # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = False         # Set to True for fast, non-animated turtle movement.

COLORS = [ "red", "green", "blue", "yellow", "cyan", "magenta", "white", "black" ]


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print( __doc__ )

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    exercise1()
    # exercise2()


class Point:
    """Point class for representing (x,y) coordinates."""

    # 0a: Read, discuss, and understand the following code.
    def __init__( self, x=0, y=0 ):
        """Create a new Point with the given x and y values.

        :param int x: The x-coordinate; default is zero.
        :param int y: The y-coordinate; default is zero.
        """
        # Assign the x and y values passed as parameters as attributes of self.
        self.x = x
        self.y = y

    def __str__( self ):
        """Return a string representation of this object.

        :return str: The point object in the format (x,y).
        """
        return "( {},{} )".format( self.x, self.y )

    # 0c: Read, discuss, and understand the following code.
    def draw( self, art ):
        """Draw this Point object using the given turtle.

        :param turtle.Turtle art: The turtle to use to draw this Point object.
        :return: None
        """
        # Use the self object's x and y values to set the heading.
        art.setheading( art.towards( self.x, self.y ) )
        # Use the self object's x and y values to move the turtle.
        art.setposition( self.x, self.y )
        # Draw a dot at the point.
        art.dot( 4 )


def exercise0():
    """Demonstrate a Point class."""
    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()
    writer.write( "Creating and drawing Point objects...",
                  align="center", font=( "Times", FONT_SIZE, "bold" ) )

    # 0b: Read, discuss, and understand the following code.
    points = []  # An empty list to be filled with Point objects.
    y = HEIGHT // 4  # Start the y-coordinate one-quarter screen height above the x-axis.
    # Loop through evenly spaced x-coordinates.
    for x in range( -WIDTH // 2 + MARGIN, WIDTH // 2 + MARGIN, ( WIDTH - MARGIN * 2 ) // 8 ):
        p = Point( x, y )  # Use the values of x and y to create a Point object.
        points.append( p )  # Appends the point to the list of point objects.
        y *= -1  # Modify y so the points alternate above and below the x-axis.

    print( " ".join( [ str( p ) for p in points ] ) )
    # 0d: Read, discuss, and understand the following code.
    # Loop through the list of Point objects and tell each to draw itself.
    for p in points:
        # Tell the Point object to draw itself using the artist turtle.
        print( p, flush=True )
        p.draw( artist )

    # Wait for the user to click before closing the window (leave this as the last line).
    artist.home()
    screen.exitonclick()


# 1a: In the space below this comment, write the class as described in the lab document.
class Spot:
    """ Spot class for ... """
    def __init__( self, c="black", x=0, y=0 ):
        """

        :param x: The x-coordinate.
        :param y: The y-coordinate.
        :param c: The color.
        """

        self.x = x
        self.y = y
        self.c = c

    def __str__(self):
        """Return a string representation of this object.

        :return str: The point object in the format (x,y):c.
        """
        return "({},{}): {}".format( self.x, self.y, self.c )

    def draw(self, art):
        """ Draw this spot object using the given turtle.

        :param turtle.Turtle art: The turtle to use to draw this spot object.
        :return:
        """

        art.setposition( self.x, self.y )
        art.dot( FONT_SIZE * 2, self.c )
        art.setposition( self.x - FONT_SIZE // 4, self.y + FONT_SIZE // 6 )
        art.dot( FONT_SIZE // 5)
        art.setposition( self.x + FONT_SIZE // 4, self.y + FONT_SIZE // 6 )
        art.dot( FONT_SIZE // 5)
        art.setposition( self.x - FONT_SIZE // 2, self.y - FONT_SIZE // 6 )
        art.setheading( 270)
        art.pd()
        art.circle( FONT_SIZE // 2, 180 )
        art.pu()
        art.setheading( 0 )


def exercise1():
    """Test a Spot class."""
    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()
    writer.write( "Creating and drawing Spot objects...",
                  align="center", font=( "Times", FONT_SIZE, "bold" ) )

    # 1b: In the space below, use the class as described in the lab document.
    x = - WIDTH // 2 + MARGIN * 2
    y = - HEIGHT // 2 + MARGIN * 2
    dx = ( WIDTH - MARGIN * 2 ) // len( COLORS)
    dy = ( HEIGHT - MARGIN * 2) // len( COLORS  )

    spots = []
    for c in COLORS:
        s = Spot( c, x, y )
        spots.append( s )
        x += dx
        y += dy

    for s in spots:
        print( s, flush=True )
        s.draw( artist )

    # Wait for the user to click before closing the window (leave this as the last line).
    artist.home()
    screen.exitonclick()


# 2a: In the space below this comment, write the class as described in the lab document.
class Raindrop:
    """Raindrop class for representing (x,y) coordinates and radius."""

    def __init__( self ):
        """Create a new Raindrop with random x and y values."""
        # Uses random.randint to create values within the margins of the turtle screen.
        self.x = random.randint( -WIDTH // 2 + MARGIN, WIDTH // 2 - MARGIN )
        self.y = random.randint( -HEIGHT // 2 + MARGIN, HEIGHT // 2 - MARGIN )
        self.r = random.randint( MARGIN, 2 * MARGIN )

    def __str__(self):
        """Return a string representation of this object.

        :return str: The point object in the format (x,y):r.
        """
        return "( {},{} ): {}".format( self.x, self.y, self.r )

    def draw( self, art ):
        """ Draw a raindrop.

        :param art: The artist to do the drawing.
        """

        art.setposition( self.x, self.y )
        art.dot( self.r * 2, "blue")

    def area( self ):
        """ Calculates and returns area of the raindrop

        :return int: Area of the Raindrop object.
        """

        return math.pi * self.r ** 2

    def overlaps( self, other ):
        """

        :param other: Another raindrop object
        :return bool: True if raindrops overlap, false if otherwise.
        """

        return math.hypot( other.x - self.x, other.y - self.y ) < other.r + self.r

    def expand( self, other, art ):
        """ Draws expanded raindrop when two are overlapping.

        :param other: Another raindrop
        :param art: Artist to do the drawing.
        """

        other.r = math.sqrt( other.r ** 2 + self.r ** 2)
        art.setposition( other.x, other.y )
        self.draw( art )


def exercise2():
    """Test a Raindrop class."""
    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()
    writer.write( "Creating and drawing Raindrop objects...",
                  align="center", font=( "Times", FONT_SIZE, "bold" ) )

    # Make the artist turtle a blue circle for this application.
    artist.color( 'blue' )
    artist.shape( "circle" )

    # 2b: In the space below, use the class as described in the lab document.
    total_area = 0
    drops = []
    while total_area < WIDTH * HEIGHT:
        new_drop = Raindrop()
        print( "Drawing new dot {}".format( new_drop ) )
        new_drop.draw( artist )

        drops.append( new_drop )
        total_area += new_drop.area()
        for other in drops[:-1]:
            if new_drop.overlaps( other ):
                print( "Expanding dot {}".format( new_drop ) )
                other.expand( other, artist )
                writer.write( "Creating and drawing Raindrop objects...",
                              align="center", font=( "Times", FONT_SIZE, "bold" ) )
                total_area += new_drop.area()

    # Wait for the user to click before closing the window (leave this as the last line).
    artist.home()
    screen.exitonclick()


def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup( WIDTH, HEIGHT, MARGIN, MARGIN )
    screen.bgcolor( "SkyBlue" )

    # Create two turtles, one for drawing and one for writing.
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape( "turtle" )
    # Lift the artist's pen and slow it down to see the movements from object to object.
    artist.penup()
    artist.speed( "slowest" )

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay( 0 )
        artist.hideturtle()
        artist.speed( "fastest" )
        writer.hideturtle()
        writer.speed( "fastest" )

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading( 90 )   # Straight up, which makes it look sort of like a cursor.
    writer.penup()            # A turtle's pen does not have to be down to write text.
    writer.setposition( 0, HEIGHT // 2 - FONT_SIZE * 2 )  # Centered at top of the screen.

    return screen, artist, writer


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
