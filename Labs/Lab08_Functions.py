"""CS 210, Introduction to Programming, Fall 2015, Tylar Hanson.

Instructor: Dr. Bower

Documentation: None
=======================================================================
"""

# Import specific functions from the math library, just to demonstrate how to do it.
from math import atan2, sqrt, degrees
import easygui
import turtle


# Define several useful constants to be used by the Turtle graphics.
WIDTH = 640              # A smaller window for this problem.
HEIGHT = WIDTH           # A square window for this problem.
MARGIN = 32              # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = 16           # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = False        # Set to True for fast, non-animated turtle movement.


def main():
    """Main program to test solutions for each problem."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print( __doc__ )

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise1()
    exercise2()
    # exercise3()


def exercise1():
    """Odd and Even Sums exercise."""
    # 1c: Write code to use the sum_odds and sum_evens functions, as described in the lab document.

    # The integer box returns an int and allows specification of lower and upper bounds.
    n = easygui.integerbox( msg="What is the end value?", title="Input", default="", lowerbound=0, upperbound=2 ** 31 )

    odd = sum_odds(n)
    even = sum_evens(n)

    # The most basic message box displays a message with an OK button.
    easygui.msgbox( " n = {} \n sum of odds = {}\n sum of evens = {}".format( n, odd, even), "Result" )


# 1a: In the space below, write the sum_odds function as described in the lab document.
def sum_odds(n):
    """Calculates and returns the sum of the odd numbers between 1 and the given value, inclusive.

    :param int n: The end value of the accumulator pattern.
    :return: Sum of the odd numbers.
    :rtype: Integer
    """

    total_odd = 0
    for i in range(1, n + 1, 2):
        total_odd += i
    return total_odd


# 1b: In the space below, write the sum_evens function as described in the lab document.
def sum_evens(n):
    """Calculates and returns the sum of the even numbers between 1 and the given value, inclusive.


    :param int n: The end value of the accumulator pattern.
    :return: Sum of the even numbers.
    :rtype: Integer
    """

    total_even = 0
    for i in range(0, n + 1, 2):
        total_even += i
    return total_even


def exercise2():
    """Summations exercise."""
    # 2b: Write code to use the summation function, as described in the lab document.

    # The integer box returns an int and allows specification of lower and upper bounds.
    n = easygui.integerbox( msg="What is the end value?", title="Input", default="42", lowerbound=0, upperbound=100 )
    power = easygui.integerbox( msg="What is the power?", title="Input", default="42", lowerbound=0, upperbound=100 )
    summation(n, power)

    # The most basic message box displays a message with an OK button.
    easygui.msgbox( " n = {}, summation( n,{} ) = {}".format(n, power, summation(n, power)) )
    easygui.msgbox( " n = {}, summation( n,{} ) = {}".format(n + 1, power, summation(n + 1, power)) )
    easygui.msgbox( " n = {}, summation( n,{} ) = {}".format(n + 2, power, summation(n + 2, power)) )


# 2a: In the space below, write the summation function as described in the lab document.
def summation(n, power):
    """The function uses the accumulator pattern (not the formula) to calculate and return the result of the summation.

    :param n: The end value of the accumulator pattern.
    :param power: The exponent for the summation.
    :return: Sum of the numbers.
    :rtype: Integer
    """

    total_sum = 0
    for i in range(0, n + 1):
        total_sum += i ** power
    return total_sum


def exercise3():
    """Use the screen and turtle defined below to solve the given exercise."""
    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()

    # TODO 3b: In the space below, write code to use the draw_inner_square function, as described in the lab document.
    draw_square(artist, 128)
    draw_inner_square( artist, 128, 32 / 100 )

    # TODO 3d: In the space below, write code to use the draw_inner_squares function, as described in the lab document.
    pass  # Remove the pass statement (and this comment) when writing your own code.

    # TODO 3f: In the space below, write code to use the draw_art function, as described in the lab document.
    pass  # Remove the pass statement (and this comment) when writing your own code.

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# TODO 3a: In the space below, write the draw_inner_square function as described in the lab document.
def draw_inner_square(tom, outer_size, percent):
    """

    :param tom: The turtle to draw the inner square.
    :param outer_size: The size of the outer square.
    :param percent: Percent used to calculate the distance.
    :return:
    """

    a = percent * outer_size
    b = outer_size - a
    inner_size = int(sqrt( a ** 2 + b ** 2 ))
    alpha = degrees( atan2( a, b ) )

    tom.left( 90 )
    tom.forward( a )
    tom.right( 90 + alpha )
    draw_square( tom, inner_size )
# TODO 3c: In the space below, write the draw_inner_squares function as described in the lab document.


# TODO 3e: In the space below, write the draw_art function as described in the lab document.


# Leave this function below those written in steps 3a, 3c, and 3e.
def draw_square( art, size ):
    """Use the given turtle to draw a square with one corner at the turtle's current location.

    :param turtle.Turtle art: The turtle to do the drawing.
    :param int size: The length of one side of the square.
    """
    for _ in range( 4 ):
        art.forward( size )
        art.left( 90 )


def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup( WIDTH, HEIGHT, MARGIN, MARGIN )
    screen.bgcolor( "SkyBlue" )

    # Create two turtles, one for drawing and one for writing.
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape( "turtle" )

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay( 0 )
        artist.hideturtle()
        artist.speed( "fastest" )
        writer.hideturtle()
        writer.speed( "fastest" )

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading( 90 )   # Straight up, which makes it look sort of like a cursor.
    writer.penup()            # A turtle's pen does not have to be down to write text.
    writer.setposition( 0, HEIGHT // 2 - FONT_SIZE * 2 )  # Centered at top of the screen.

    return screen, artist, writer


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
