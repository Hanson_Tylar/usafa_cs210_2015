"""CS 210, Introduction to Programming, Fall 2015, Tylar Hanson.

Instructor: Dr. Bower

Documentation: None
=======================================================================
"""

import math
import random
import turtle

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960               # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 // 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = WIDTH // 30      # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = MARGIN // 2   # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = False         # Set to True for fast, non-animated turtle movement.

COLORS = [ "red", "green", "blue", "yellow", "cyan", "magenta", "white", "black" ]


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print( __doc__ )

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    # exercise1()
    exercise2()


# 0a: Read, discuss, and understand the following code.
class Point:
    """Point class for representing (x,y) coordinates."""

    def __init__( self ):
        """Create a new Point with random x and y values."""
        # Uses random.randint to create values within the margins of the turtle screen.
        self.x = random.randint( -WIDTH // 2 + MARGIN, WIDTH // 2 - MARGIN )
        self.y = random.randint( -HEIGHT // 2 + MARGIN, HEIGHT // 2 - MARGIN )


def exercise0():
    """Demonstrate a Point class."""
    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()

    # 0b: Read, discuss, and understand the following code.
    points = []  # An empty list to be filled with Point objects.
    for _ in range( 8 ):
        p = Point()  # Calls the Point object's __init__ method to create a point object.
        points.append( p )  # Appends the point to the list of point objects.

    # Loop through the list of Point objects and use the artist turtle to draw them.
    for p in points:
        # Have the writer turtle display the Point object's x and y values.
        writer.clear()
        writer.write( "Moving to point ({}, {})...".format( p.x, p.y ),
                      align="center", font=( "Times", FONT_SIZE, "bold" ) )

        # Use the Point object's x and y values to set the heading.
        artist.setheading( artist.towards( p.x, p.y ) )
        # Use the Point object's x and y values to move the turtle.
        artist.setposition( p.x, p.y )
        # Draw a dot at the point.
        artist.dot( 4 )

    # Put things back when finished.
    writer.clear()
    artist.home()
    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# 1a: In the space below this comment, write the class as described in the lab document.
class Spot:
    """Spot class for representing (x,y) coordinates and a color."""

    def __init__( self ):
        """Create a new Point with random x and y values and a random color choice."""
        # Uses random.randint to create values within the margins of the turtle screen.
        self.x = random.randint( -WIDTH // 2 + MARGIN, WIDTH // 2 - MARGIN )
        self.y = random.randint( -HEIGHT // 2 + MARGIN, HEIGHT // 2 - MARGIN )
        self.c = random.choice( COLORS )


def exercise1():
    """Test a Spot class."""
    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()

    # 1b: In the space below, use the class as described in the lab document.
    spots = []  # An empty list to be filled with Point objects.
    for _ in range( 8 ):
        s = Spot()  # Calls the Spot object's __init__ method to create a point object.
        spots.append( s )  # Appends the spot to the list of spot objects.

    # Loop through the list of Spot objects and use the artist turtle to draw them.
    for s in spots:
        # Have the writer turtle display the Spot object's x and y values.
        writer.clear()
        writer.write( "Draw a {} spot at ({}, {})...".format( s.c, s.x, s.y ),
                      align="center", font=( "Times", FONT_SIZE, "bold" ) )

        # Use the Spot object's x and y values to set the heading.
        artist.setheading( artist.towards( s.x, s.y ) )
        # Use the Spot object's x and y values to move the turtle.
        artist.setposition( s.x, s.y )
        # Draw a dot at the spot.
        artist.dot( 32, s.c )

    # Put things back when finished.
    writer.clear()
    artist.home()
    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# 2a: In the space below this comment, write the class as described in the lab document.
class Raindrop:
    """Raindrop class for representing (x,y) coordinates and radius."""

    def __init__( self ):
        """Create a new Raindrop with random x and y values."""
        # Uses random.randint to create values within the margins of the turtle screen.
        self.x = random.randint( -WIDTH // 2 + MARGIN, WIDTH // 2 - MARGIN )
        self.y = random.randint( -HEIGHT // 2 + MARGIN, HEIGHT // 2 - MARGIN )
        self.r = random.randint( MARGIN, 2 * MARGIN )


def exercise2():
    """Test a Raindrop class."""
    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()

    # 2b: In the space below, use the class as described in the lab document.
    drops = []  # An empty list to be filled with Raindrop objects.
    total_area = 0

    # Loop through the list of Raindrop objects and use the artist turtle to draw them.
    while total_area < WIDTH * HEIGHT:
        new_drop = Raindrop()  # Calls the Raindrop object's __init__ method to create a point object.
        # Have the writer turtle display the Raindrop object's x and y values and radius.
        writer.clear()
        writer.write( "Draw a Raindrop at ({}, {}) of radius {} ...".format( new_drop.x, new_drop.y, new_drop.r ),
                      align="center", font=( "Times", FONT_SIZE, "bold" ) )

        # Use the Raindrop object's x and y values to set the heading.
        artist.setheading( artist.towards( new_drop.x, new_drop.y ) )
        # Use the Raindrop object's x and y values to move the turtle.
        artist.setposition( new_drop.x, new_drop.y )
        # Draw a dot at the spot.
        artist.dot( 2 * new_drop.r, "blue" )
        drops.append( new_drop )
        total_area +=  math.pi * new_drop.r ** 2

        for old_drop in drops[:-1]:
            if math.hypot( old_drop.x - new_drop.x, old_drop.y - new_drop.y ) <= old_drop.r + new_drop.r:
                old_drop.r = math.sqrt( old_drop.r ** 2 + new_drop.r ** 2)
                artist.setposition( old_drop.x, old_drop.y )
                artist.dot( 2 * old_drop.r, "blue" )
                total_area += math.pi * old_drop.r ** 2

    # Put things back when finished.
    writer.clear()
    artist.home()
    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup( WIDTH, HEIGHT, MARGIN, MARGIN )
    screen.bgcolor( "SkyBlue" )

    # Create two turtles, one for drawing and one for writing.
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape( "turtle" )
    # Lift the artist's pen and slow it down to see the movements from object to object.
    artist.penup()
    artist.speed( "slowest" )

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay( 0 )
        artist.hideturtle()
        artist.speed( "fastest" )
        writer.hideturtle()
        writer.speed( "fastest" )

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading( 90 )   # Straight up, which makes it look sort of like a cursor.
    writer.penup()            # A turtle's pen does not have to be down to write text.
    writer.setposition( 0, HEIGHT // 2 - FONT_SIZE * 2 )  # Centered at top of the screen.

    return screen, artist, writer


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
