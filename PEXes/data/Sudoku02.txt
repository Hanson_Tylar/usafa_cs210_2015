# A sample, solved puzzle.
# The data is on one lines because file reading often needs
# to treat any/all white space the same, so here's practice.
#
# Also, a few extra lines of comments to make this interesting.
#
# Hooah!
#
3 9 6 4 1 5 2 7 8 1 2 5 7 3 8 4 6 9 4 7 8 2 6 9 3 1 5 7 5 9 6 4 2 8 3 1 8 4 3 5 9 1 7 2 6 2 6 1 3 8 7 5 9 4 5 3 4 9 2 6 1 8 7 6 8 7 1 5 3 9 4 2 9 1 2 8 7 4 6 5 3
