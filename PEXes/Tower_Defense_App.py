"""CS 210, Introduction to Programming, Fall 2015, Tylar Hanson.

Instructor: Dr. Bower

Documentation: None

This is a Tower Defense Game, similar to the popular game Bloons TD. The idea is
to place towers that kill enemies before they get to the other end of the map.
If enough enemies get through your defenses you lose the game.

Below are basic aspects of the game.

Most basic: http://www.newgrounds.com/portal/view/546595
-	One enemy type
-	One tower type
-	No upgrades
-	One map, straight line paths
Less basic: http://www.freeworldgroup.com/games6/gameindex/simpletowerdefense3.htm
-	Multiple tower types; varying damage, attack rate, and range
-	Upgradeable towers; increase rate, range, and damage
-	Can sell towers and be refunded.
More advanced: http://armorgames.com/play/12141/kingdom-rush
-	Multiple enemy types;  varying health levels, speed, and resistances
-	Special abilities; one time use to damage enemies.
-	Enemies move along curved paths

=======================================================================
"""

import math
import sys

from PyQt4 import QtCore, QtGui

from PEXes.TowerDefenseGUI import Ui_TowerDefenseGUI as Gui


def main():
    """Launch a GUI created with Qt Designer."""

    # Create a QApplication to handle event processing.
    qt_app = QtGui.QApplication( sys.argv )

    # Create an instance of the app and show the main window.
    my_app = App()
    my_app.main_window.show()

    # Execute the QApplication, exiting when it returns (i.e., the window is closed).
    sys.exit( qt_app.exec_() )  # Note the underscore at the end of exec_().


class Player( object ):
    """Player class to represent a player with Life and Money"""

    def __init__( self, life=100, money=100 ):
        """Create a new Player with the given x and y values.

        :param int life: The players life points. Game is over when this reaches zero; default is one-hundred.
        :param int money: The money used to buy towers; default is one-hundred.
        """
        # All Enemy objects have x and y attributes for their current position on the map.
        self.life = life
        self.money = money

    def __str__( self ):
        """Build and return a string representation of the object.

        :return str: A string representation of this Player.
        """
        return "Life: {}     Money: {}".format( self.life, self.money )

    def lose_life( self ):
        """Subtract 1 from number of lives each time an enemy goes off the map."""

        self.life -= 1

    def make_money( self, enemy ):
        """Player makes money when an Enemy dies.

        :param Enemy Object enemy: The enemy that died.
        """
        self.money += enemy.value

    def spend_money( self, tower ):
        """

        :param Tower Object tower: The tower that Player is trying to buy
        """
        self.money -= tower.cost


class Enemy( object ):
    """Class to represent an enemy with (x,y) coordinates.

    This will serve as the parent class for more specific sub classes.
    """
    SIZE = 24

    def __init__( self, x=0, y=0, health=50, value=0, speed=5 ):
        """Create a new Enemy with the given x and y values.

        :param int x: The x-coordinate; default is zero.
        :param int y: The y-coordinate; default is zero.
        :param int health: How much damage an enemy can take before it dies.
        :param int value: This is how much money Player gains when Enemy object is killed.
        """
        # All Enemy objects have x and y attributes for their current position on the map.
        self.x, self.y = x, y
        self.health = health
        self.maxhealth = health
        self.value = value
        self.speed = speed

        self.fill_color = QtCore.Qt.blue
        self.line_color = QtCore.Qt.darkGray

    def __str__( self ):
        """Build and return a string representation of the object.

        :return str: A string representation of this Enemy in the format "(x,y)".
        """
        return "({},{})".format( self.x, self.y )

    def draw( self, painter ):
        """Draw this Enemy object using the given QPainter.

        :param QtGui.QPainter painter: The QPainter object to do the drawing.
        :return: None
        """
        # All enemies are drawn as filled ellipses.
        painter.setBrush( self.fill_color )
        painter.setPen( self.line_color )
        painter.drawEllipse( self.x - Enemy.SIZE // 2, self.y - Enemy.SIZE // 2, Enemy.SIZE, Enemy.SIZE )
        # Draw enemy's health bar
        painter.setBrush( QtCore.Qt.red )
        painter.setPen( QtCore.Qt.red )
        painter.drawRect( self.x - Enemy.SIZE // 2, self.y - Enemy.SIZE // 2 - 10, Enemy.SIZE, 5 )
        painter.setBrush( QtCore.Qt.green )
        painter.setPen( QtCore.Qt.green )
        painter.drawRect( self.x - Enemy.SIZE // 2, self.y - Enemy.SIZE // 2 - 10,
                          Enemy.SIZE * self.health // self.maxhealth, 5 )

    def move( self, points ):
        """An Enemy moves from one point of the map's path to another.

        :param lst points: List of points that enemy will move along.
        :return: None
        """
        self.x += self.speed
        # If self.x is less than 0 than self.y would be a negative index of the points list.
        # It needs to start from the beginning
        self.y = float(points[self.x].split()[1]) if self.x >= 0 else int(points[0].split()[1])

    def lose_health( self, damage ):
        """Subtract health points from enemies when they are hit by towers.

        :param int damage:  How many points to take away from the Enemy's health.
        """
        self.health -= damage
        if self.health < 0:
            self.health = 0


class Normal( Enemy ):
    """Class to represent a Normal Enemy."""

    def __init__( self, x, y ):
        """Create a new Normal Enemy with the given x and y values.

        :param int x: The x-coordinate; default is zero.
        :param int y: The y-coordinate; default is zero.
        """
        # Pass the x and y values up to the super class constructor.
        super().__init__( x, y )

        # Normal Enemies are red with a white outline.
        self.fill_color = QtCore.Qt.red
        self.line_color = QtCore.Qt.white

        # Normal Enemies have the following attributes.
        self.speed = 5
        self.value = 5
        self.health = 50
        self.maxhealth = 50


class Heavy( Enemy ):
    """Class to represent a Heavy Enemy."""

    def __init__( self, x, y ):
        """Create a new Heavy Enemy with the given x and y values.

        :param int x: The x-coordinate; default is zero.
        :param int y: The y-coordinate; default is zero.
        """
        # Pass the x and y values up to the super class constructor.
        super().__init__( x, y )

        # Normal Enemies are red with a white outline.
        self.fill_color = QtCore.Qt.yellow
        self.line_color = QtCore.Qt.white

        # Normal Enemies have the following attributes.
        self.speed = 2
        self.value = 10
        self.health = 500
        self.maxhealth = 500


class Tower( object ):
    """Class to represent a Tower object."""

    SIZE = 32

    def __init__( self, x=0, y=0, damage=0, attack_range=0, cost=0 ):
        """Create a new Tower with the given x and y values.

        :param int x: The x-coordinate; default is zero.
        :param int y: The y-coordinate; default is zero.
        :param int damage: How much damage the tower does per hit; default is zero.
        :param int attack_range: The max distance a tower can shoot an enemy at; default is zero.
        :param int cost: How much money it costs to buy a tower; default is zero.
        """
        # All Tower objects have x and y attributes for their position on the map.
        self.x, self.y = x, y
        # All Tower objects have damage, attack range, and cost attributes.
        self.damage, self.attack_range = damage, attack_range
        self.cost = cost
        self.target = None

        self.fill_color = QtCore.Qt.blue
        self.line_color = QtCore.Qt.darkGray

    def find_target( self, tower, enemies ):
        """Target the first enemy in range of the tower.

        :param Tower object tower: The tower that needs to be given a target.
        :param lst enemies: List of enemies to choose a target from.
        :return Enemy object: Returns an Enemy object to be targeted if one is in range, else None.
        """
        for i in range( len( enemies ) ):
            if math.hypot( enemies[i].x - tower.x, enemies[i].y - tower.y ) < tower.attack_range:
                self.target = enemies[i]
                return enemies[i]


class Laser( Tower ):
    """Class to represent a Laser Tower."""

    def __init__( self, x, y ):
        """Create a new Laser Tower with the given x and y values.

        :param int x: The x-coordinate; default is zero.
        :param int y: The y-coordinate; default is zero.
        """
        # Pass the x and y values up to the super class constructor.
        super().__init__( x, y )
        self.damage, self.attack_range = 3, 75
        self.cost = 50

        # Laser Towers are blue with a black outline.
        self.fill_color = QtGui.QColor( 30, 144, 255 )
        self.line_color = QtCore.Qt.black

    def draw( self, painter ):
        """Draw this Laser Tower object using the given QPainter.

        :param QtGui.QPainter painter: The QPainter object to do the drawing.
        :return: None
        """
        # Laser Towers are drawn as filled ellipses and a laser beam if the tower is targeting an enemy.
        painter.setBrush( self.fill_color )
        painter.setPen( self.line_color )
        painter.drawEllipse( self.x - Tower.SIZE // 2, self.y - Tower.SIZE // 2, Tower.SIZE, Tower.SIZE )
        if self.target:
            if math.hypot( self.target.x - self.x, self.target.y - self.y ) < self.attack_range:
                painter.setPen( QtGui.QPen( QtGui.QColor( 0, 255, 255 ), Tower.SIZE // 6 ) )
                painter.drawLine( self.x, self.y, self.target.x, self.target.y )
                self.target = None


class Turret( Tower ):
    """Class to represent a Turret."""

    def __init__( self, x, y ):
        """Create a new Turrets with the given x and y values.

        :param int x: The x-coordinate; default is zero.
        :param int y: The y-coordinate; default is zero.
        """
        # Pass the x and y values up to the super class constructor.
        super().__init__( x, y )

        # Turrets are gray with a black outline.
        self.fill_color = QtGui.QColor( 105, 105, 105 )
        self.line_color = QtCore.Qt.black
        self.damage, self.attack_range = 20, 100
        self.cost = 50

    def draw( self, painter ):
        """Draw this Turret object using the given QPainter.

        :param QtGui.QPainter painter: The QPainter object to do the drawing.
        :return: None
        """
        # Turrets are drawn as filled ellipses and a laser beam if the tower is targeting an enemy.
        painter.setBrush( self.fill_color )
        painter.setPen( self.line_color )
        painter.drawEllipse( self.x - Tower.SIZE // 2, self.y - Tower.SIZE // 2, Tower.SIZE, Tower.SIZE )
        if self.target:
            if math.hypot( self.target.x - self.x, self.target.y - self.y ) < self.attack_range:
                pen = QtGui.QPen(QtGui.QColor( 178, 34, 34 ), Tower.SIZE // 6, QtCore.Qt.DashDotDotLine)
                painter.setPen( pen )
                painter.drawLine( self.x, self.y, self.target.x, self.target.y )
                self.target = None


class Map( object ):
    """Class to represent the map and it's path."""

    PATH_SIZE = Enemy.SIZE * 3 // 2

    def __init__( self ):
        """Build a map with a path following the given points."""

    def draw_path( self, painter, points ):
        """Draw the path that Enemy objects will follow.

        :param QtGui.QPainter painter: The QPainter object to do the drawing.
        :param lst points: List of points that the path will follow.
        """
        painter.setPen( QtGui.QPen( QtGui.QColor( 255, 228, 196 ), Map.PATH_SIZE ) )
        for i in range( len( points ) - 1 ):
            x1, y1 = int(points[i].split()[0]), float(points[i].split()[1])
            x2, y2 = int(points[i + 1].split()[0]), float(points[i + 1].split()[1])
            painter.drawLine( x1, y1, x1, y2 )


class App( object ):
    """Application class to create and control the gui."""

    def __init__( self ):
        """Initialize the gui."""

        # Create the main window in which our gui will display.
        self.main_window = QtGui.QMainWindow()  # Widget()  # QMainWindow() for menu and status bar.

        # Create an instance of our gui and set it up in the main window.
        self.gui = Gui()  # Generic name "Gui" from "as Gui" clause of import statement.
        self.gui.setupUi( self.main_window )
        self.w, self.h = self.gui.drawing_widget.width(), self.gui.drawing_widget.height()
        self.current_wave = 0

        # Connect user actions to methods.
        self.gui.actionExit.triggered.connect( self.main_window.close )
        self.gui.actionNew_Wave.triggered.connect( self.new_wave )
        self.gui.actionPause.triggered.connect( self.pause )
        self.gui.TowerGroup.buttonClicked.connect( self.button_clicked )
        self.gui.drawing_widget.mousePressEvent = self.mouse_press

        # Catch the paint event so the drawing widget can be redrawn.
        self.gui.drawing_widget.paintEvent = self.paint_event

        self.enemies = []
        self.player = Player( 50, 200 )
        self.towers = []
        self.game_over = False
        self.current_button = None
        self.points = []
        with open( "Map2.txt" ) as data_file:
            data_lines = data_file.read().splitlines()
            for line in data_lines:
                self.points.append( " ".join(line.split( sep='\t') ))

        # Create a QTimer object to control the animations.
        self.timer1 = QtCore.QTimer()
        self.timer2 = QtCore.QTimer()
        # Connect the move_enemies and towers_shoot methods so they execute when the timer goes off.
        self.timer1.timeout.connect( self.move_enemies )
        self.timer1.timeout.connect( self.lasers_shoot )
        self.timer2.timeout.connect( self.turrets_shoot )
        # The timer goes off every 33 milliseconds; roughly a 30 frames-per-second animation.
        if not self.game_over:
            self.timer1.start( 33 )
            self.timer2.start( 500 )

    def new_wave( self ):
        """Create a new list of enemies for the current wave."""

        # Prevent user from spamming too many waves at once.
        if len( self.enemies ) < 200 and not self.game_over:
            i = 0
            enemy_count = self.current_wave * 3 if self.current_wave > 0 else 3
            if self.current_wave < 3:
                for _ in range( enemy_count ):
                    self.enemies.append( Normal( int(self.points[0].split()[0]) - Enemy.SIZE * ( i + 1 ),
                                                 int(self.points[0].split()[1] ) ) )
                    i += 2
            elif self.current_wave < 6:
                for _ in range( enemy_count ):
                    self.enemies.append( Normal( int(self.points[0].split()[0]) - Enemy.SIZE * ( i + 1 ),
                                                 int(self.points[0].split()[1] ) ) )
                    i += 1
            else:
                for _ in range( enemy_count // 2 ):
                    self.enemies.append( Normal( int(self.points[0].split()[0]) - Enemy.SIZE * ( i + 1 ),
                                                 int(self.points[0].split()[1] ) ) )
                    self.enemies.append( Heavy( int(self.points[0].split()[0]) - Enemy.SIZE * ( i + 2 ),
                                                int(self.points[0].split()[1] ) ) )
                    i += 2
            self.current_wave += 1

    def pause( self ):
        """Pause or continue the game."""

        # If timers are active, stop them; otherwise, start them.
        if self.timer1.isActive():
            self.timer1.stop()
            self.timer2.stop()
        else:
            self.timer1.start( 33 )  # Roughly a 30 frames-per-second animation.
            self.timer2.start( 66 )

    def lasers_shoot( self ):
        """Towers shoot any Enemy within it's attack_range."""

        if len( self.enemies ) > 0 and not self.game_over:
            for tower in self.towers:
                if type(tower).__name__ == "Laser":
                    my_target = Tower.find_target( tower, tower, self.enemies )
                    if my_target is not None:
                        my_target.lose_health( tower.damage )

    def turrets_shoot( self ):
        """Towers shoot any Enemy within it's attack_range."""

        if len( self.enemies ) > 0 and not self.game_over:
            for tower in self.towers:
                if type(tower).__name__ == "Turret":
                    my_target = Tower.find_target( tower, tower, self.enemies )
                    if my_target is not None:
                        my_target.lose_health( tower.damage )

    def move_enemies( self ):
        """Move all the enemies, then update the drawing widget."""

        # Pass the current width and height of the drawing widget to each enemy as it moves.
        w, h = self.gui.drawing_widget.width(), self.gui.drawing_widget.height()

        if len( self.enemies ) > 0 and not self.game_over:
            for i in range( len( self.enemies ) ):
                try:
                    self.enemies[i].move( self.points )
                    if self.enemies[i].x > w:
                        self.player.lose_life()
                        self.game_over = False if self.player.life > 0 else True
                        del self.enemies[i]
                    if self.enemies[i].health <= 0:
                        Player.make_money(  self.player, self.enemies[i] )
                        del self.enemies[i]
                except IndexError:
                    pass
        # Update the drawing widget, which causes the system to call paint_event.
        self.gui.drawing_widget.update()
        self.gui.statusbar.showMessage( "Wave Number: {}     Enemies remaining: {}          {}"
                                        .format( self.current_wave, len(self.enemies), self.player ) )

    def mouse_press( self, event ):
        """Called automatically when the user presses the mouse button on the drawing widget.

        :param PyQt.QtGui.QMouseEvent event: The event object from PyQt.
        :return: None
        """
        x, y = event.x(), event.y()
        can_draw = True
        if not self.game_over and self.current_button:
            desired_tower = self.current_button( x, y )
            if desired_tower.cost <= self.player.money:
                # Keep Towers from being placed on top of one another.
                for other in self.towers:
                    if math.hypot( desired_tower.x - other.x, desired_tower.y - other.y) < Tower.SIZE:
                        can_draw = False
                    # Towers can not be placed on top of the path.
                    for point in self.points:
                        x_point = int( point.split()[0] )
                        y_point = float( point.split()[1] )
                        distance_to_path = math.hypot( x - x_point, y - y_point )
                        if distance_to_path < Map.PATH_SIZE // 2 + Tower.SIZE // 2:
                            can_draw = False
                if can_draw:
                    self.player.spend_money( desired_tower )
                    self.towers.append( desired_tower )

    def button_clicked( self, button ):
        """Called automatically when any button in the Constellation button group is clicked.

        :param QtGui.QRadioButton button: The button that was clicked.
        """

        tower_type = button.text()
        if tower_type == "Laser":
            self.current_button = Laser
        else:
            self.current_button = Turret

    def paint_event( self, q_paint_event ):
        """Called automatically whenever the drawing widget needs to repaint.

        :param PyQt.QtGui.QPaintEvent q_paint_event: The event object from PyQt (not used).
        """
        # Get a QPainter object that can paint on the drawing widget.
        painter = QtGui.QPainter( self.gui.drawing_widget )

        self.w, self.h = self.gui.drawing_widget.width(), self.gui.drawing_widget.height()

        Map.draw_path( None, painter, self.points )

        for enemy in self.enemies:
            enemy.draw( painter )

        for tower in self.towers:
            tower.draw( painter )

        if self.game_over:
            painter.drawRect( self.w // 2 - 200, self.h // 2 - 100, 400, 200 )
            painter.setPen( QtGui.QPen( QtGui.QColor( 0, 0, 0 ) ) )
            font = painter.font()
            font.setPointSize( 40 )
            painter.setFont( font )
            painter.drawText( self.w // 2 - 150, self.h // 2, "GAME OVER")


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
