"""CS 210, Introduction to Programming, Fall 2015, Tylar Hanson .

Instructor: Dr. Bower

Documentation: None.
"""

import easygui
import random
import turtle

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960              # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 / 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = 32              # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = 16           # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True         # Set to True for fast, stealthy turtles.
SIZE = ( WIDTH - ( 9 * MARGIN ) ) / 8


def main():
    """Main program to implement the game PIG."""
    # Create the turtle screen and two turtles (leave this as the first line of main).
    screen, artist, writer = turtle_setup()

    player1 = easygui.enterbox( msg="Enter Player 1 name:", title="Input!", default="Malcolm Reynolds" )
    player2 = easygui.enterbox( msg="Enter Player 2 name:", title="Input!", default="Jayne Cobb" )
    player1_score = 0
    player2_score = 0
    writer.write( "{}:  {},  {}:  {}".format(player1, player1_score, player2, player2_score), align="center",
                  font=( "Courier", FONT_SIZE, "bold"))

    while player1_score < 100 and player2_score < 100:              # End the game if either score is 100 or greater
        player1_score += roll_die( artist, player1, player1_score )
        writer.clear()
        artist.clear()
        writer.write( "{}:  {},  {}:  {}".format(player1, player1_score, player2, player2_score),
                      align="center", font=( "Courier", FONT_SIZE, "bold"))
        if player1_score < 100 and player2_score < 100:
            player2_score += roll_die( artist, player2, player2_score )
            writer.clear()
            artist.clear()
            writer.write( "{}:  {},  {}:  {}".format(player1, player1_score, player2, player2_score),
                          align="center", font=( "Courier", FONT_SIZE, "bold"))

    if player1_score > player2_score:                               # Display winner in center of the screen
        writer.goto( 0, 0 )
        writer.write( "{} Wins!".format(player1), align="center", font=( "Courier", FONT_SIZE, "bold"))
    else:
        writer.goto( 0, 0 )
        writer.write( "{} Wins!".format(player2), align="center", font=( "Courier", FONT_SIZE, "bold"))

    # Wait for the user to click before closing the window (leave this as the last line of main).
    screen.exitonclick()


# : Implement your roll_die function here.
def roll_die( tom, player, total_score):
    """Interacts with user to execute one turn of the game.

    :param turtle.Turtle tom: The turtle to do the drawing.
    :param str player: Current player for the turn.
    :param int total_score: Player's current score.
    :return: Integer for the score for the turn.
    """

    score = 0                                           # Sets score for current players turn.
    # The following lines rolls the first die of each players turn automatically.
    x = -4                                              # Start the first die at top left.
    y = 1
    num_pips = random.randint( 1, 6 )
    draw_die( tom, num_pips, x * ( SIZE + MARGIN ), y * ( SIZE + 20 ) )

    if num_pips != 1:
        score += num_pips
        total_score += num_pips
    else:
        easygui.msgbox( "{} scores {}.".format(player, score), ok_button='Ok' )
        return 0
    if total_score < 100:                               # Is total score under 100 after first die roll.
        response = easygui.boolbox("{}'s current score is {}. Roll or Hold?".format(player, score),
                                   "Input", choices=[ "Roll", "Hold" ] )
    else:
        easygui.msgbox( "{} scores {}.".format(player, score), ok_button='Ok' )
        return score

    while num_pips != 1 and total_score < 100 and response:
        if x == 3:                                      # Changes coordinate of the die.
            x = -4
            y -= 1
        else:
            x += 1
        num_pips = random.randint( 1, 6 )
        draw_die( tom, num_pips, x * ( SIZE + MARGIN), y * ( SIZE + 20 ) )
        if num_pips != 1:
            score += num_pips
            total_score += num_pips
            if total_score < 100:
                response = easygui.boolbox("{}'s current score is {}. Roll or Hold?"
                                           .format(player, score), "Input", choices=[ "Roll", "Hold" ] )
    if num_pips == 1:
        easygui.msgbox("{} scores 0.".format( player ), ok_button='Ok.')
        return 0
    else:
        easygui.msgbox("{} scores {}.".format( player, score ), ok_button='Ok.')
        return score


# : Implement your draw_die function here.
def draw_die( tom, num_pips, x, y):
    """Function to draw a single die and the pips.

    :param turtle.Turtle tom: The turtle to do the drawing.
    :param int num_pips: Number of pips to draw.
    :param int x: X coordinate of where the die should be drawn
    :param int y: Y coordinate of where the die should be drawn
    """

    radius = SIZE / 10                                      # Radius of curved corner of the die.
    tom.pu()                                                # Go to location and draw a  red square.
    tom.goto( x + radius, y )
    tom.pd()
    tom.color("black", "red")
    tom.begin_fill()
    for _ in range(4):
        tom.forward( SIZE - 2 * radius )
        tom.circle( radius, 360 / 4 )
    tom.end_fill()
    tom.pu()

    if num_pips % 2 != 0:                                   # If dice number is odd.
        tom.goto( x + SIZE / 2, y + SIZE / 2 )
        tom.dot( SIZE / 7 )                                 # Make center pip.
    if num_pips > 1:
        tom.goto( x + 2.5 * SIZE / 10, y + 8 * SIZE / 10 )  # Draw top left pip.
        tom.dot( SIZE / 7 )
        tom.goto( x + 7.5 * SIZE / 10, y + 2 * SIZE / 10 )  # Draw bottom right pip.
        tom.dot( SIZE / 7 )
    if num_pips > 3:
        tom.goto( x + 7.5 * SIZE / 10, y + 8 * SIZE / 10 )  # Draw top right pip.
        tom.dot( SIZE / 7 )
        tom.goto( x + 2.5 * SIZE / 10, y + 2 * SIZE / 10 )  # Draw bottom left pip.
        tom.dot( SIZE / 7 )
    if num_pips == 6:
        tom.goto( x + 2.5 * SIZE / 10, y + 5 * SIZE / 10 )  # Draw mid left pip.
        tom.dot( SIZE / 7 )
        tom.goto( x + 7.5 * SIZE / 10, y + 5 * SIZE / 10 )  # Draw mid right pip.
        tom.dot( SIZE / 7 )


def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup( WIDTH, HEIGHT, MARGIN, MARGIN )
    screen.bgcolor( "Green" )

    # Create two turtles, one for drawing and one for writing.
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape( "turtle" )

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay( 0 )
        artist.hideturtle()
        artist.speed( "fastest" )
        writer.hideturtle()
        writer.speed( "fastest" )

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading( 90 )   # Straight up, which makes it look sort of like a cursor.
    writer.penup()            # A turtle's pen does not have to be down to write text.
    writer.setposition( 0, HEIGHT // 2 - FONT_SIZE * 2 )  # Centered at top of the screen.

    return screen, artist, writer

# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
