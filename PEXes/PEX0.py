"""CS 210, Introduction to Programming, Fall 2015, Tylar Hanson.

Instructor: Dr. Bower

Documentation: None.
"""

import turtle
import math

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960                             # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 / 16                 # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = 32                             # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = 16                          # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True                        # Set to True for fast, stealthy turtles.
SIZE = ( WIDTH - 4 * MARGIN ) // 3      # The size of one side of the square and triangle. The diameter of the circle.
fill_level = (turtle.numinput("Input", "Enter fill percentage: ", 69, minval=50, maxval=95)) / 100
"""Pop up a dialog window for input of a number and assign to variable fill_level.

:param title: The title of the dialog window.
:param prompt: Text mostly describing what numerical information to input.
:param default: The default value.
:param minval: The minimum value for input.
:param maxval: The maximum value for input.
"""


def main():
    """Main program to test solution for PEX0."""
    # Create the turtle screen and two turtles.
    screen, yertle, writer = turtle_setup()

    draw_square(yertle, -1 * MARGIN - 1.5 * SIZE, -SIZE // 2, SIZE)
    draw_circle(yertle, 0, -SIZE // 2, SIZE)
    draw_triangle(yertle, 1 * MARGIN + .5 * SIZE, -SIZE // 2, SIZE)
    draw_fill_line(yertle)
    calc_area(writer)

    # Wait for the user to click before closing the window (leave this as the last line of main).
    screen.exitonclick()


def draw_square( tom, x, y, size ):
    """Use the given turtle to draw a square with one corner at coordinate (x,y).

    Note: The orientation of the square is dependent on the heading of the turtle.
    If the turtle's heading is zero, the (x,y) coordinate is the lower-left corner.

    :param turtle.Turtle tom: The turtle to do the drawing.
    :param int x: The x-coordinate of one corner of the square.
    :param int y: The y-coordinate of one corner of the square.
    :param int size: The length of one side of the square.
    """
    # Lift the pen and move to the indicated position.
    tom.penup()
    tom.setposition( x, y )
    tom.pendown()

    # Draw the square.
    for _ in range( 4 ):
        tom.forward( size )
        tom.left( 90 )


def draw_circle( tom, x, y, size ):
    """Use the given turtle to draw a circle with one endpoint at coordinate (x,y).

    Note: The orientation of the circle is dependent on the heading of the turtle.
    If the turtle's heading is zero, the (x,y) coordinate is the lowest endpoint.

    :param turtle.Turtle tom: The turtle to do the drawing.
    :param int x: The x-coordinate of one endpoint of the circle.
    :param int y: The y-coordinate of one endpoint of the circle.
    :param int size: The diameter of the circle.
    """
    # Lift the pen and move to the indicated position.
    tom.penup()
    tom.setposition( x, y )
    tom.pendown()

    # Draw the circle.
    tom.circle( size // 2 )


def draw_triangle( tom, x, y, size ):
    """Use the given turtle to draw a triangle with one corner at coordinate (x,y).

    Note: The orientation of the triangle is dependent on the heading of the turtle.
    If the turtle's heading is zero, the (x,y) coordinate is the lower-left corner.

    :param turtle.Turtle tom: The turtle to do the drawing.
    :param int x: The x-coordinate of one corner of the triangle.
    :param int y: The y-coordinate of one corner of the triangle.
    :param int size: The length of one side of the triangle.
    """
    # Lift the pen and move to the indicated position.
    tom.penup()
    tom.setposition( x, y )
    tom.pendown()

    # Draw the triangle.
    tom.forward( size )
    tom.left( 180 - ( 180 - 90 ) // 2 )
    tom.forward( math.sqrt( size ** 2 + size ** 2 ))
    tom.left( 180 - ( 180 - 90 ) // 2 )
    tom.forward( size )
    tom.left( 90 )


def draw_fill_line( tom ):
    """Use the given turtle to draw a line at coordinate (x,y).

    Note: The orientation of the line is dependent on the heading of the turtle.
    If the turtle's heading is zero, the (x,y) coordinate is the left point.

    :param turtle.Turtle tom: The turtle to do the drawing.
    """

    # Lift the pen and move to left edge of square at fill level.
    tom.penup()
    tom.setposition( -1 * MARGIN - 1.5 * SIZE, ( -SIZE // 2 ) + fill_level * SIZE )
    tom.pendown()

    # Distance from edge of circle to point perpendicular to the center
    b = math.sqrt( ( ( SIZE // 2 ) ** 2 ) - ( (fill_level - .5 ) * SIZE ) ** 2)

    # Draw the line.
    # Draw SIZE + 1 because the line comes out one pixel short in final drawing.
    tom.forward( SIZE + 1 )
    tom.pu()
    # Go to left edge of circle.
    tom.goto( -b, ( -SIZE // 2 ) + fill_level * SIZE )
    tom.pd()
    # Draw line in circle
    tom.forward( 2 * b )
    tom.pu()
    # Go to left edge of triangle
    tom.goto( 1 * MARGIN + .5 * SIZE + 1, ( -SIZE // 2 ) + fill_level * SIZE )
    tom.pd()
    # Draw line in triangle
    tom.forward( ( 1 - fill_level ) * SIZE )


def calc_area( tom ):
    """Calculate the area of the regions under the fill line.

    :param turtle.turtle tom: The turtle to do the writing.
    """

    area_square = SIZE ** 2 * fill_level

    # R = radius of the circle
    # h = Distance from origin of circle to chord line ( fill line )
    r = SIZE / 2
    h = fill_level * SIZE - r
    area_circle = math.pi * r ** 2 - ( r ** 2 * math.acos( h / r ) - h * math.sqrt( r ** 2 - h ** 2))

    area_triangle = SIZE ** 2 / 2 - ( SIZE - ( fill_level * SIZE )) ** 2 / 2

    # Print calculated area with writer turtle.
    tom.setposition( -SIZE - MARGIN, -HEIGHT // 2 + FONT_SIZE * 6 )
    tom.write( "{:,.2f}".format( area_square ), align="center", font=( "Courier", FONT_SIZE, "bold" ) )
    tom.setposition( 0, -HEIGHT // 2 + FONT_SIZE * 6 )
    tom.write( "{:,.2f}".format( area_circle ), align="center", font=( "Courier", FONT_SIZE, "bold" ) )
    tom.setposition( SIZE + MARGIN, -HEIGHT // 2 + FONT_SIZE * 6 )
    tom.write( "{:,.2f}".format( area_triangle ), align="center", font=( "Courier", FONT_SIZE, "bold" ) )


def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing yertle.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup( WIDTH, HEIGHT, MARGIN, MARGIN )
    screen.bgcolor( "SkyBlue" )

    # Create two turtles, one for drawing and one for writing.
    yertle = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the yertle turtle's shape so yertle and writer are distinguishable.
    image = "YERTLE.gif"
    screen.register_shape(image)
    yertle.shape(image)

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay( 0 )
        yertle.hideturtle()
        yertle.speed( "fastest" )
        writer.hideturtle()
        writer.speed( "fastest" )

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading( 90 )   # Straight up, which makes it look sort of like a cursor.
    writer.penup()            # A turtle's pen does not have to be down to write text.
    writer.setposition( 0, -HEIGHT // 2 + FONT_SIZE * 6 )  # Centered at bottom of the screen.

    return screen, yertle, writer


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()

