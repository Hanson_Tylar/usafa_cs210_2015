"""CS 210, Introduction to Programming, Fall 2015, Tylar Hanson.

Instructor: Dr. Bower

Documentation: None
=======================================================================
"""

from PEXes.PEX3_StarGui import Ui_PEX3_StarGui as Gui
from PyQt4 import QtCore, QtGui
import sys
import math


def main():
    """Launch a GUI created with Qt Designer."""

    # Create a QApplication to handle event processing.
    qt_app = QtGui.QApplication( sys.argv )

    # Create an instance of the app and show the main window.
    my_app = App()
    my_app.main_window.show()

    # Execute the QApplication, exiting when it returns (i.e., the window is closed).
    sys.exit( qt_app.exec_() )  # Note the underscore at the end of exec_().


class App:
    """Application class to create and control the gui."""

    def __init__( self ):
        """Initialize the gui."""

        # Create the main window in which our gui will display.
        self.main_window = QtGui.QMainWindow()  # QMainWindow() for menu and status bar.

        # Create an instance of our gui and set it up in the main window.
        self.gui = Gui()  # Generic name "Gui" from "as Gui" clause of import statement.
        self.gui.setupUi( self.main_window )

        # Get the width, height, and scale of the drawing widget.
        self.w, self.h = self.gui.drawing_widget.width(), self.gui.drawing_widget.height()
        self.scale = self.w if self.w < self.h else self.h

        # Connect the buttons and mouse press to methods in this application.
        self.gui.action_exit.triggered.connect( self.main_window.close )
        self.gui.action_open_2.triggered.connect( self.open_map )
        self.gui.action_about.triggered.connect( self.about )
        self.gui.action_Name.triggered.connect( self.find_by_name )
        self.gui.action_Harvard_Revised.triggered.connect( self.find_by_harvard )
        self.gui.action_Henry_Draper.triggered.connect( self.find_by_henry )
        self.gui.Constellation_group.buttonClicked.connect( self.button_clicked )
        self.gui.drawing_widget.paintEvent = self.paint_event
        self.gui.drawing_widget.mousePressEvent = self.mouse_press_event

        # All Star and Constellation objects are kept in a list.
        self.stars = []
        self.constellation_BigDipper = Constellation( "BigDipper" )
        self.constellation_Bootes = Constellation( "Bootes" )
        self.constellation_Cassiopeia = Constellation( "Cassiopeia" )
        self.constellation_Cygnet = Constellation( "Cygnet" )
        self.constellation_Gemini = Constellation( "Gemini" )
        self.constellation_Hydra = Constellation( "Hydra" )
        self.constellation_Test = Constellation( "Test" )
        self.constellation_UrsaMajor = Constellation( "UrsaMajor" )
        self.constellation_UrsaMinor = Constellation( "UrsaMinor" )

        # The current Star and Constellation, if one has been clicked.
        self.current_star = None
        self.current_constellation = None

        with open( "./data/PEX3_Data/Stars_All.txt" ) as data_file:
            star_data_lines = data_file.read().splitlines()
        for line in star_data_lines:
            self.stars.append( Star( line ) )

    def open_map( self ):
        """Clears the star map, loads the stars in the star data file, and then draws the new star map."""

        self.stars = []
        self.current_star, self.current_constellation = None, None
        filename = QtGui.QFileDialog.getOpenFileName( directory="./data/PEX3_Data", filter="Stars_*")
        with open( filename ) as data_file:
            star_data_lines = data_file.read().splitlines()
        for line in star_data_lines:
            self.stars.append( Star( line ) )

    def about( self ):
        """Shows the about dialog."""

        QtGui.QMessageBox.about( self.main_window, "About", "CS 210, Fall 2015, Star Map\n\nAuthor: Tylar Hanson" )

    def find_by_name( self ):
        """Function to find a star by name obtained from user input."""

        name = QtGui.QInputDialog.getText( None, 'Input Dialog', 'Enter a name:' )
        for star in self.stars:
            if len( star.data ) > 6:
                if name[0].upper() in star.name or name[0].upper() in star.name.split( sep=';'):
                    self.current_star = star
        self.gui.drawing_widget.update()
        self.gui.statusbar.showMessage( "Star at ({}, {}): {}".format( ( self.current_star.x + 1 ) * self.scale // 2,
                                                                       ( -self.current_star.y + 1 ) * self.scale // 2,
                                                                       self.current_star ) )

    def find_by_harvard( self ):
        """Function to find a star by Harvard Revised Number obtained from user input."""

        number = QtGui.QInputDialog.getText( None, 'Input Dialog', 'Enter Harvard Revised Number:' )
        for star in self.stars:
            if star.Harvard_Revised == int( number[ 0 ] ):
                self.current_star = star

        self.gui.drawing_widget.update()
        self.gui.statusbar.showMessage( "Star at ({}, {}): {}".format( ( self.current_star.x + 1 ) * self.scale // 2,
                                                                       ( -self.current_star.y + 1 ) * self.scale // 2,
                                                                       self.current_star ) )

    def find_by_henry( self ):
        """Function to find a star by Henry Draper Number obtained from user input."""

        number = QtGui.QInputDialog.getText( None, 'Input Dialog', 'Enter Henry Draper Number:' )
        for star in self.stars:
            if star.Henry_Draper == int( number[ 0 ] ):
                self.current_star = star

        self.gui.drawing_widget.update()
        self.gui.statusbar.showMessage( "Star at ({}, {}): {}".format( ( self.current_star.x + 1 ) * self.scale // 2,
                                                                       ( -self.current_star.y + 1 ) * self.scale // 2,
                                                                       self.current_star ) )

    def button_clicked( self, button ):
        """Called automatically when any button in the Constellation button group is clicked.

        :param QtGui.QRadioButton button: The button that was clicked.
        """

        if button.text() == "Big Dipper":
            self.current_constellation = self.constellation_BigDipper
        elif button.text() == "Bootes":
            self.current_constellation = self.constellation_Bootes
        elif button.text() == "Cassiopeia":
            self.current_constellation = self.constellation_Cassiopeia
        elif button.text() == "Cygnet":
            self.current_constellation = self.constellation_Cygnet
        elif button.text() == "Gemini":
            self.current_constellation = self.constellation_Gemini
        elif button.text() == "Hydra":
            self.current_constellation = self.constellation_Hydra
        elif button.text() == "Test":
            self.current_constellation = self.constellation_Test
        elif button.text() == "Ursa Major":
            self.current_constellation = self.constellation_UrsaMajor
        elif button.text() == "Ursa Minor":
            self.current_constellation = self.constellation_UrsaMinor
        else:
            self.current_constellation = None

        self.gui.drawing_widget.update()
        self.gui.statusbar.showMessage( "Constellation {}".format( button.text() ) )

    def mouse_press_event( self, event ):
        """Called automatically when the drawing widget is clicked.

        :param QtGui.QMouseEvent event: An event object created by the system.
        """

        # Get the x and y coordinates of the click point.
        x, y = event.x(), event.y()

        # Find the Star object nearest to the click.
        index = 0
        self.current_star = self.stars[ 1 ]

        while index < len( self.stars ):
            if math.hypot( x - ( self.stars[ index ].x + 1 ) * self.scale // 2,
                           y - ( -self.stars[ index ].y + 1 ) * self.scale // 2 ) < \
                    math.hypot( x - ( self.current_star.x + 1 ) * self.scale // 2,
                                y - ( -self.current_star.y + 1 ) * self.scale // 2 ):
                self.current_star = self.stars[ index ]
            index += 1
        self.gui.drawing_widget.update()
        self.gui.statusbar.showMessage( "Star at ({}, {}): {}".format( ( self.current_star.x + 1 ) * self.scale // 2,
                                                                       ( -self.current_star.y + 1 ) * self.scale // 2,
                                                                       self.current_star ) )

    def paint_event( self, event ):
        """Called automatically when the drawing widget need to repaint.

        :param QtGui.QPaintEvent event: An event object created by the system.
        """

        # Get a QPainter object that can  paint on the drawing widget.
        painter = QtGui.QPainter( self.gui.drawing_widget )

        self.w, self.h = self.gui.drawing_widget.width(), self.gui.drawing_widget.height()
        self.scale = self.w if self.w < self.h else self.h

        for star in self.stars:
            Star.draw( star, painter, self.scale )

        if self.current_star is not None:
            # Draw current star in red to indicate it is the current star.
            painter.setBrush( QtCore.Qt.red )  # The brush determines the fill color.
            painter.setPen( QtCore.Qt.red )    # The pen determines the outline color.
            # The parameters to drawEllipse are the (x,y) of the upper-left corner and width, height.
            painter.drawEllipse( ( self.current_star.x + 1 ) * self.scale // 2 - 1,
                                 ( ( self.current_star.y * -1 ) + 1 ) * self.scale // 2 - 1, 2, 2 )
        if self.current_constellation is not None:
            # Draw current constellation in red to indicate it is the current constellation.
            painter.setBrush( QtCore.Qt.red )  # The brush determines the fill color.
            painter.setPen( QtCore.Qt.red )    # The pen determines the outline color.
            Constellation.draw( self.current_constellation, painter, self.scale, self.stars )


class Star:
    """Class for representing a Star object"""

    def __init__( self, star_data_line ):
        """Create a Star object with attributes assigned from given string.

        :param str star_data_line: String containing attributes for current star.
        """

        self.data = star_data_line.split()
        self.x, self.y, self.magnitude = float( self.data[ 0 ] ), float( self.data[ 1 ] ), float( self.data[ 3 ] )
        self.Henry_Draper, self.Harvard_Revised = int( self.data[ 4 ] ), int( self.data[ 5 ] )

        if len( self.data ) == 7:
            self.name = self.data[ 6 ]
        if len( self.data ) > 7:
            self.name = " ".join( self.data[6:])

    def __str__( self ):
        """Build and return a string representation of the object.

        :return str: A string representation of this Star .
        """

        if len( self.data ) > 6:
            return "{} {} {} {} {} {}".format( self.x, self.y, self.magnitude,
                                               self.Henry_Draper, self.Harvard_Revised, self.name )
        else:
            return "{} {} {} {} {}".format( self.x, self.y, self.magnitude, self.Henry_Draper, self.Harvard_Revised )

    def draw( self, painter, scale ):
        """ Uses the QPainter to draw the star, calculating the x and y pixel coordinate.

        :param QtGui.QPainter painter: The QPainter object to do the drawing.
        :param int scale: Scale of the drawing widget.
        """

        gray = 255 * 2.512 ** ( -self.magnitude ) * 10
        gray = 255 if gray > 255 else 0 if gray < 0 else gray

        painter.setBrush( QtGui.QColor( gray, gray, gray ) )  # The brush determines the fill color.
        painter.setPen( QtGui.QColor( gray, gray, gray ) )    # The pen determines the outline color.
        painter.drawEllipse( ( self.x + 1 ) * scale // 2 - 1, ( ( -self.y ) + 1 ) * scale // 2 - 1, 2, 2 )


class Constellation:
    """Class for representing a constellation object"""

    def __init__( self, filename ):
        """Create a Constellation object with attributes assigned from given filename.

        :param str filename: Name of a file containing names of stars in the constellation.
        """

        self.constellation = []
        filename = "./data/PEX3_Data/Constellation_" + filename.replace( " ", "" ) + ".txt"
        with open( filename ) as data_file:
            lines = data_file.read().splitlines()
        for line in lines:
            self.constellation.append( line )

    def __str__(self):
        """Build and return a string representation of the object.

        :return str: String that can be read to reconstruct the Constellation Object.
        """

        return self.constellation

    def draw( self, painter, scale, stars ):
        """Uses the QPainter to draw the constellation.

        :param QtGui.QPainter painter: The QPainter object to do the drawing.
        :param int scale: Scale of the drawing widget.
        :param lst stars: List of star objects that make up the constellation.
        """

        for line in self.constellation:
            name1, name2 = str(line).split( sep=",")[0], str(line).split( sep=",")[1]
            for star in stars:
                if len( star.data ) > 6:
                    if name1 in star.name.split( sep=';'):
                        x1, y1 = star.x, star.y
                    if name2 in star.name.split( sep=';'):
                        x2, y2 = star.x, star.y
            painter.drawLine((x1 + 1) * scale // 2, (-y1 + 1) * scale // 2,
                             (x2 + 1) * scale // 2, (-y2 + 1) * scale // 2)

# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
