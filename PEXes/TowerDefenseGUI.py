# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:/Users/C18Tylar.Hanson/Documents/Fall 2015/CS 210/USAFA_CS210_2015/PEXes/TowerDefenseGUI.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_TowerDefenseGUI(object):
    def setupUi(self, TowerDefenseGUI):
        TowerDefenseGUI.setObjectName(_fromUtf8("TowerDefenseGUI"))
        TowerDefenseGUI.resize(909, 511)
        TowerDefenseGUI.setMaximumSize(QtCore.QSize(909, 16777215))
        self.centralwidget = QtGui.QWidget(TowerDefenseGUI)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.main_layout = QtGui.QHBoxLayout()
        self.main_layout.setObjectName(_fromUtf8("main_layout"))
        self.drawing_widget = QtGui.QWidget(self.centralwidget)
        self.drawing_widget.setMinimumSize(QtCore.QSize(800, 450))
        self.drawing_widget.setMaximumSize(QtCore.QSize(800, 450))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 140, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 140, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 140, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 140, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        self.drawing_widget.setPalette(palette)
        self.drawing_widget.setAutoFillBackground(True)
        self.drawing_widget.setObjectName(_fromUtf8("drawing_widget"))
        self.main_layout.addWidget(self.drawing_widget)
        self.vertical_line = QtGui.QFrame(self.centralwidget)
        self.vertical_line.setFrameShape(QtGui.QFrame.VLine)
        self.vertical_line.setFrameShadow(QtGui.QFrame.Sunken)
        self.vertical_line.setObjectName(_fromUtf8("vertical_line"))
        self.main_layout.addWidget(self.vertical_line)
        self.controls_layout = QtGui.QVBoxLayout()
        self.controls_layout.setObjectName(_fromUtf8("controls_layout"))
        self.Tower_layout = QtGui.QVBoxLayout()
        self.Tower_layout.setObjectName(_fromUtf8("Tower_layout"))
        self.Towers_label = QtGui.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.Towers_label.setFont(font)
        self.Towers_label.setObjectName(_fromUtf8("Towers_label"))
        self.Tower_layout.addWidget(self.Towers_label)
        self.laserButton = QtGui.QRadioButton(self.centralwidget)
        self.laserButton.setObjectName(_fromUtf8("laserButton"))
        self.TowerGroup = QtGui.QButtonGroup(TowerDefenseGUI)
        self.TowerGroup.setObjectName(_fromUtf8("TowerGroup"))
        self.TowerGroup.addButton(self.laserButton)
        self.Tower_layout.addWidget(self.laserButton)
        self.turretButton = QtGui.QRadioButton(self.centralwidget)
        self.turretButton.setObjectName(_fromUtf8("turretButton"))
        self.TowerGroup.addButton(self.turretButton)
        self.Tower_layout.addWidget(self.turretButton)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.Tower_layout.addItem(spacerItem)
        self.horizontal_line = QtGui.QFrame(self.centralwidget)
        self.horizontal_line.setFrameShape(QtGui.QFrame.HLine)
        self.horizontal_line.setFrameShadow(QtGui.QFrame.Sunken)
        self.horizontal_line.setObjectName(_fromUtf8("horizontal_line"))
        self.Tower_layout.addWidget(self.horizontal_line)
        self.controls_layout.addLayout(self.Tower_layout)
        self.main_layout.addLayout(self.controls_layout)
        self.verticalLayout.addLayout(self.main_layout)
        TowerDefenseGUI.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(TowerDefenseGUI)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 909, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuMenu = QtGui.QMenu(self.menubar)
        self.menuMenu.setObjectName(_fromUtf8("menuMenu"))
        TowerDefenseGUI.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(TowerDefenseGUI)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        TowerDefenseGUI.setStatusBar(self.statusbar)
        self.actionNew_Wave = QtGui.QAction(TowerDefenseGUI)
        self.actionNew_Wave.setObjectName(_fromUtf8("actionNew_Wave"))
        self.actionExit = QtGui.QAction(TowerDefenseGUI)
        self.actionExit.setObjectName(_fromUtf8("actionExit"))
        self.actionPause = QtGui.QAction(TowerDefenseGUI)
        self.actionPause.setObjectName(_fromUtf8("actionPause"))
        self.menuMenu.addAction(self.actionNew_Wave)
        self.menuMenu.addAction(self.actionExit)
        self.menuMenu.addAction(self.actionPause)
        self.menubar.addAction(self.menuMenu.menuAction())

        self.retranslateUi(TowerDefenseGUI)
        QtCore.QMetaObject.connectSlotsByName(TowerDefenseGUI)

    def retranslateUi(self, TowerDefenseGUI):
        TowerDefenseGUI.setWindowTitle(_translate("TowerDefenseGUI", "TowerDefenseGUI", None))
        self.Towers_label.setText(_translate("TowerDefenseGUI", "Towers", None))
        self.laserButton.setText(_translate("TowerDefenseGUI", "Laser", None))
        self.turretButton.setText(_translate("TowerDefenseGUI", "Turret", None))
        self.menuMenu.setTitle(_translate("TowerDefenseGUI", "Menu", None))
        self.actionNew_Wave.setText(_translate("TowerDefenseGUI", "New Wave", None))
        self.actionNew_Wave.setShortcut(_translate("TowerDefenseGUI", "Return", None))
        self.actionExit.setText(_translate("TowerDefenseGUI", "Exit", None))
        self.actionExit.setShortcut(_translate("TowerDefenseGUI", "Esc", None))
        self.actionPause.setText(_translate("TowerDefenseGUI", "Pause", None))
        self.actionPause.setShortcut(_translate("TowerDefenseGUI", "Space", None))

