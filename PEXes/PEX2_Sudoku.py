"""CS 210, Introduction to Programming, Fall 2015, Tylar Hanson.

Instructor: Dr. Bower

Documentation: None.
"""

import easygui   # For easygui.fileopenbox.
import os        # For os.path.basename.

from PEXes.PEX2_Puzzles import PUZZLE, PUZZLE_SOLVED, PUZZLE_INVALID, DATA, DATA_SOLVED


def main():
    """Main program to do Sudoku, so here's a Sudoku haiku:

    One through nine in place
    To open the matrix door
    Let logic guide you
    """
    # 1b: Test the str_sudoku function with the given puzzles.
    # print( str_sudoku( PUZZLE ) )
    # print( str_sudoku( PUZZLE_SOLVED ) )

    # 2b: Test the print_sudoku function with the given puzzles.
    # print_sudoku( PUZZLE )
    # print_sudoku( PUZZLE_SOLVED )

    # 3b: Test the create_sudoku puzzle with the given data.
    # print_sudoku( create_sudoku( DATA ) )
    # print_sudoku( create_sudoku( DATA_SOLVED ) )

    # 4b: Test the open_sudoku function with the given files.
    # print_sudoku( open_sudoku( "./data/Sudoku_Blank.txt" ) )
    # print_sudoku( open_sudoku( "./data/Sudoku00.txt" ) )
    # print_sudoku( open_sudoku( "./data/Sudoku01.txt" ) )
    # print_sudoku( open_sudoku( "./data/Sudoku02.txt" ) )

    # 5b: Test the is_solved function with the given puzzles.
    # print( is_solved( PUZZLE ), is_solved( PUZZLE_SOLVED ) )

    # 6b: Test the is_valid function with the given puzzles.
    # print("is_valid")
    # print("Puzzle, Puzzle_solved, Puzzle Invalid")
    # print( is_valid( PUZZLE ), is_valid( PUZZLE_SOLVED ), is_valid( PUZZLE_INVALID ) )

    # 7: Implement the main program as described in the PEX document.
    filename = True
    while filename is not None:
        print()
        filename = easygui.fileopenbox( "Choose a puzzle", "Input", default="./data/*.txt" )
        puzzle = open_sudoku(filename)
        if is_valid(puzzle):
            if is_solved(puzzle):
                print_sudoku(puzzle)
                print("The puzzle IS valid and IS solved.")
            else:
                print_sudoku(puzzle)
                print("The puzzle IS valid and is NOT solved.")
                print("\nSolving . . .")
                solve_sudoku(puzzle)
                print_sudoku(puzzle)

        else:
            print_sudoku(puzzle)
            print("The puzzle is NOT valid and is NOT solved")


# 1a: Implement the str_sudoku function as described in the PEX document.
def str_sudoku( puzzle ):
    """Creates a string of puzzle values suitable for printing to a file.

    The string created by this function is formatted such that it could be
    passed to create_sudoku function and recreate the same puzzle.

    Note: This function DOES NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: A string suitable for printing to a file or passing to create_sudoku.
    """
    data_str = ""
    for r in range(len(puzzle)):
        for c in range(len(puzzle[r])):
            data_str += "{} ".format(str(puzzle[r][c]))
        data_str += "\n"
    return data_str


# 2a: Implement the print_sudoku function as described in the PEX document.
def print_sudoku( puzzle, fancy=True ):
    """Prints the nested list structure in pretty rows and columns.

    For example, PEX2_Puzzles.PUZZLE would print as follows:
    +===+===+===+===+===+===+===+===+===+
    # 1 | 8 |   # 6 |   | 9 #   | 5 | 7 #
    +---+---+---+---+---+---+---+---+---+
    # 5 |   |   #   |   |   #   |   | 3 #
    +---+---+---+---+---+---+---+---+---+
    #   |   | 2 #   | 8 |   # 4 |   |   #
    +===+===+===+===+===+===+===+===+===+
    # 7 |   |   # 8 | 4 | 5 #   |   | 1 #
    +---+---+---+---+---+---+---+---+---+
    #   |   | 3 # 2 |   | 1 # 9 |   |   #
    +---+---+---+---+---+---+---+---+---+
    # 2 |   |   # 9 | 6 | 3 #   |   | 5 #
    +===+===+===+===+===+===+===+===+===+
    #   |   | 1 #   | 5 |   # 8 |   |   #
    +---+---+---+---+---+---+---+---+---+
    # 8 |   |   #   |   |   #   |   | 4 #
    +---+---+---+---+---+---+---+---+---+
    # 9 | 4 |   # 3 |   | 8 #   | 7 | 2 #
    +===+===+===+===+===+===+===+===+===+

    Note: This function DOES NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :param bool fancy: Print with fancy ASCII Art; default is True.
    :return: None
    """
    border1 = "+===+===+===+===+===+===+===+===+===+"
    border2 = "+---+---+---+---+---+---+---+---+---+"
    for r in range(len(puzzle)):
        if r % 3 == 0:
            print("\n{}".format(border1))
        else:
            print("\n{}".format(border2))
        for c in range(len(puzzle[r])):
            if c % 3 == 0:
                print("# {} ".format(str(puzzle[r][c]) if puzzle[r][c] != 0 else " "), end="")
            elif c == 8:
                print("| {} #".format(str(puzzle[r][c]) if puzzle[r][c] != 0 else " "), end="")
            else:
                print("| {} ".format(str(puzzle[r][c]) if puzzle[r][c] != 0 else " "), end="")
    print("\n" + border1)
    print()


# 3a: Implement the create_sudoku function as described in the PEX document.
def create_sudoku( data ):
    """Creates a 9x9 nested list of integers from a string with 81 separate values.

    :param str data: A string with 81 separate integer values, [0-9].
    :return: The Sudoku puzzle as a 9x9 nested list of integers.
    :rtype: list[list[int]]
    """
    # Turn a string into a list of integers.
    result = []
    for str_value in data.split():
        result.append( int( str_value ) )
    # Put result into a 9 x 9 nested list.
    matrix = []
    i = 0
    for _ in range(9):
        row = result[i:i + 9]
        matrix.append(row)
        i += 9
    return matrix


# 4a: Implement the open_sudoku function as described in the PEX document.
def open_sudoku( filename ):
    """Opens the given file, parses the contents, and returns a Sudoku puzzle.

    This function prints to the console any comment lines in the file.

    :param str filename: The file name.
    :return: The Sudoku puzzle as a 9x9 nested list of integers.
    :rtype: list[list[int]]
    """
    print(os.path.basename( filename ))
    print()
    with open(filename) as data_file:
        data = data_file.read().splitlines()
        while data[0].startswith('#'):
            print(data[0].strip('# '))
            data = data[ 1: ]
    return create_sudoku(" ".join(data).strip("\n").strip(" "))


# 5a: Implement the is_solved function as described in the PEX document.
def has_duplicates( a_list ):
    """Determines if a given list contains duplicate values other than zero.

    :param list a_list: A list of values.
    :return bool: True if the list contains duplicate values other than zero; False otherwise
    """
    b_list = a_list.copy()
    for _ in range(b_list.count(0)):
        b_list.remove(0)
    for b_value in a_list:
        if b_list.count( b_value ) > 1:
            return True
    return False


def is_solved( puzzle ):
    """Determines if a Sudoku puzzle is valid and complete.

    Note: This function DOES NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: True if the puzzle is valid and complete; False otherwise.
    """

    # Puzzle must be valid to be solved.
    if is_valid(puzzle):
        # If puzzle contains any zeroes it is not solved.
        for r in range(len(puzzle)):
            for c in range(len(puzzle[r])):
                if puzzle[r][c] == 0:
                    return False
        return True

    return False


# 6a: Implement the is_valid function as described in the PEX document.
def is_valid( puzzle ):
    """Determines if a Sudoku puzzle is valid, but not necessarily complete.

    Note: This function DOES NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: True if the puzzle is valid; False otherwise.
    """
    # If any row or columns contains duplicate numbers it is not valid.

    # Check all rows
    for r in range(len(puzzle)):
        if has_duplicates(puzzle[r]):
            return False
    # Check all columns
    for c in range(len(puzzle)):
        column = [puzzle[r][c] for r in range(len(puzzle))]
        if has_duplicates(column):
            return False

    # If any 3 x 3 square has duplicates it is not valid.

    left_grid = []
    mid_grid = []
    right_grid = []
    for i in range(9):
        left_grid += puzzle[i][0:3]
        mid_grid += puzzle[i][3:6]
        right_grid += puzzle[i][6:9]
        if i == 2 or i == 5 or i == 8:
            if has_duplicates(left_grid) or has_duplicates(mid_grid) or has_duplicates(right_grid):
                return False
            left_grid = []
            mid_grid = []
            right_grid = []

    # Passed all other tests, return True
    return True


# 8: Implement the solve_sudoku function as discussed in class (lesson 20 and/or 21).
def solve_sudoku( puzzle, cell=0, value=1 ):
    """Recursive function to solve a Sudoku puzzle with brute force.

    Note: This function DOES modify the puzzle!!!

    :param list[list[int]] puzzle:  The 9x9 nested list of integers.
    :param int cell: The cell of the current value being solved, [0-80].
    :param int value: The value to try in the current cell, [1-9].
    :return: None
    """

    r = cell // 9
    c = cell % 9
    if not is_solved( puzzle ) and is_valid( puzzle ) and cell <= 80 and value <= 9:
        if puzzle[r][c] != 0:
            solve_sudoku( puzzle, cell + 1, 1 )
        else:
            puzzle[r][c] = value
            solve_sudoku( puzzle, cell + 1, 1)

            if not is_solved( puzzle ):
                puzzle[r][c] = 0
                solve_sudoku( puzzle, cell, value + 1 )

    return None


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
