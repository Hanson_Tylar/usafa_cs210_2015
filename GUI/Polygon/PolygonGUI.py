# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:/Users/C18Tylar.Hanson/Documents/Fall 2015/CS 210/USAFA_CS210_2015/GUI/Polygon/PolygonGUI.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_PolygonGUI(object):
    def setupUi(self, PolygonGUI):
        PolygonGUI.setObjectName(_fromUtf8("PolygonGUI"))
        PolygonGUI.resize(640, 480)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        PolygonGUI.setPalette(palette)
        PolygonGUI.setAutoFillBackground(True)
        self.verticalLayout_2 = QtGui.QVBoxLayout(PolygonGUI)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.main_layout = QtGui.QVBoxLayout()
        self.main_layout.setObjectName(_fromUtf8("main_layout"))
        self.top_layout = QtGui.QHBoxLayout()
        self.top_layout.setObjectName(_fromUtf8("top_layout"))
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.top_layout.addItem(spacerItem)
        self.drawing_widget = QtGui.QWidget(PolygonGUI)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        self.drawing_widget.setPalette(palette)
        self.drawing_widget.setAutoFillBackground(True)
        self.drawing_widget.setObjectName(_fromUtf8("drawing_widget"))
        self.top_layout.addWidget(self.drawing_widget)
        self.main_layout.addLayout(self.top_layout)
        self.bottom_layout = QtGui.QHBoxLayout()
        self.bottom_layout.setObjectName(_fromUtf8("bottom_layout"))
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.bottom_layout.addItem(spacerItem1)
        self.triangle_button = QtGui.QPushButton(PolygonGUI)
        self.triangle_button.setObjectName(_fromUtf8("triangle_button"))
        self.bottom_layout.addWidget(self.triangle_button)
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.bottom_layout.addItem(spacerItem2)
        self.square_button = QtGui.QPushButton(PolygonGUI)
        self.square_button.setObjectName(_fromUtf8("square_button"))
        self.bottom_layout.addWidget(self.square_button)
        spacerItem3 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.bottom_layout.addItem(spacerItem3)
        self.parallelogram_button = QtGui.QPushButton(PolygonGUI)
        self.parallelogram_button.setObjectName(_fromUtf8("parallelogram_button"))
        self.bottom_layout.addWidget(self.parallelogram_button)
        spacerItem4 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.bottom_layout.addItem(spacerItem4)
        self.radio_layout = QtGui.QVBoxLayout()
        self.radio_layout.setObjectName(_fromUtf8("radio_layout"))
        self.replace_radio = QtGui.QRadioButton(PolygonGUI)
        self.replace_radio.setObjectName(_fromUtf8("replace_radio"))
        self.radio_layout.addWidget(self.replace_radio)
        self.insert_radio = QtGui.QRadioButton(PolygonGUI)
        self.insert_radio.setObjectName(_fromUtf8("insert_radio"))
        self.radio_layout.addWidget(self.insert_radio)
        self.bottom_layout.addLayout(self.radio_layout)
        spacerItem5 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.bottom_layout.addItem(spacerItem5)
        self.main_layout.addLayout(self.bottom_layout)
        self.verticalLayout_2.addLayout(self.main_layout)

        self.retranslateUi(PolygonGUI)
        QtCore.QMetaObject.connectSlotsByName(PolygonGUI)

    def retranslateUi(self, PolygonGUI):
        PolygonGUI.setWindowTitle(_translate("PolygonGUI", "PolygonGUI", None))
        self.triangle_button.setText(_translate("PolygonGUI", "Triangle", None))
        self.square_button.setText(_translate("PolygonGUI", "Square", None))
        self.parallelogram_button.setText(_translate("PolygonGUI", "Parallelogram", None))
        self.replace_radio.setText(_translate("PolygonGUI", "Replace closest point", None))
        self.insert_radio.setText(_translate("PolygonGUI", "Insert before closest point", None))

