# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'TemperatureGUI.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Temperature(object):
    def setupUi(self, Temperature):
        Temperature.setObjectName(_fromUtf8("Temperature"))
        Temperature.resize(400, 300)
        self.horizontalLayout_4 = QtGui.QHBoxLayout(Temperature)
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.left_layout = QtGui.QHBoxLayout()
        self.left_layout.setObjectName(_fromUtf8("left_layout"))
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.Fahrenheit_lineEdit = QtGui.QLineEdit(Temperature)
        self.Fahrenheit_lineEdit.setObjectName(_fromUtf8("Fahrenheit_lineEdit"))
        self.verticalLayout_2.addWidget(self.Fahrenheit_lineEdit)
        self.label = QtGui.QLabel(Temperature)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout_2.addWidget(self.label)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem1)
        self.left_layout.addLayout(self.verticalLayout_2)
        self.horizontalLayout_4.addLayout(self.left_layout)
        self.center_layout = QtGui.QHBoxLayout()
        self.center_layout.setObjectName(_fromUtf8("center_layout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem2)
        self.F_to_C_button = QtGui.QPushButton(Temperature)
        self.F_to_C_button.setObjectName(_fromUtf8("F_to_C_button"))
        self.verticalLayout.addWidget(self.F_to_C_button)
        self.C_to_F_button = QtGui.QPushButton(Temperature)
        self.C_to_F_button.setObjectName(_fromUtf8("C_to_F_button"))
        self.verticalLayout.addWidget(self.C_to_F_button)
        spacerItem3 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem3)
        self.center_layout.addLayout(self.verticalLayout)
        self.horizontalLayout_4.addLayout(self.center_layout)
        self.rightlayout = QtGui.QHBoxLayout()
        self.rightlayout.setObjectName(_fromUtf8("rightlayout"))
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        spacerItem4 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem4)
        self.Celsius_lineEdit = QtGui.QLineEdit(Temperature)
        self.Celsius_lineEdit.setObjectName(_fromUtf8("Celsius_lineEdit"))
        self.verticalLayout_3.addWidget(self.Celsius_lineEdit)
        self.Celsius_label = QtGui.QLabel(Temperature)
        self.Celsius_label.setObjectName(_fromUtf8("Celsius_label"))
        self.verticalLayout_3.addWidget(self.Celsius_label)
        spacerItem5 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem5)
        self.rightlayout.addLayout(self.verticalLayout_3)
        self.horizontalLayout_4.addLayout(self.rightlayout)

        self.retranslateUi(Temperature)
        QtCore.QMetaObject.connectSlotsByName(Temperature)

    def retranslateUi(self, Temperature):
        Temperature.setWindowTitle(_translate("Temperature", "Temperature Converter", None))
        self.label.setText(_translate("Temperature", "Fahrenheit Temperature", None))
        self.F_to_C_button.setText(_translate("Temperature", "F to C --->", None))
        self.C_to_F_button.setText(_translate("Temperature", "C to F --->", None))
        self.Celsius_label.setText(_translate("Temperature", "Celsius Temperature", None))

